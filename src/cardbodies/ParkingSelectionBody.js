import React, { Fragment } from 'react';
import { 
	Button,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import axios from 'axios';

const ParkingSelectionBody = (props) => {
	console.log("PARKING BODY", props.token)
	
	// const { _id, name, description, ratingValue, res01, res02, res03 } = props.dog

	// const url = "http://localhost:8000"

	// const updateRating = async (body) => {
	// 	try {
	// 		console.log(body)
	// 		const res = await axios.post(`${url}/:id`, body)
	// 		//SWAL
	// 	} catch(e) {
	// 		//SWAL
	// 		console.log(e)
	// 	}
	// }

	return (
		<Fragment>
			<Card className="mb-5">
				<CardBody>
					<CardTitle><h1 className="text-center card-hdr">PARKING TITLE</h1></CardTitle>
					<CardSubtitle className=""><h5>DESCRIPTION</h5></CardSubtitle>
				</CardBody>
					<img className="img-fluid w-100 row no-gutters mt-n3" src="" />
				<CardBody>
					<CardText className="text-center">parking title</CardText>
					<Button className="ml-1 btn btn-block btn-warning">Rate Me!</Button>
				</CardBody>
			</Card>
		</Fragment>
		)
}

export default ParkingSelectionBody;