import React, { Fragment, useState, useEffect } from 'react';
import {
	Button,
	Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody,
	Table, 
} from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import moment from 'moment';

const BookingModalUser = (props) => {
	
	const {
		modal,
		toggle,
		index,
		booking,
		getBooking,
		roleId,
		token,
		pendingBooking,
		approveBooking,
		declineBooking,
		payBooking,
		unpayBooking,
		completeBooking,
		ongoingBooking,
		archiveBooking,
		unarchiveBooking,
		deleteBooking,
		enableParking,
		setLoading
	} = props

	const [ disabledBtn, setDisabledBtn ] = useState(false)
	const [ disabledDeclineBtn, setDisabledDeclineBtn ] = useState(true)

	// console.log("BOOKING DATA TYPE", typeof booking)
	// console.log("BOOKING DATA", booking)	
	// console.log("BOOKING USER" ,booking.userId)

	const [bookingData, setBookingData] = useState({})

	useEffect(()=>{
		booking ? setBookingData(booking) : console.log("2 ++++")
		// setBookingData(booking)
		// console.log("BOOKING USER" ,booking.userId)
	}, [booking])

	console.log("BOOKING DATA", booking)	

	useEffect(() => {
		setBookingData(booking)
	}, [toggle])


// 	const onClickPendingHandler = async (e) => {
//   e.preventDefault();
//   const updates = {
//     statusId
//   }
//   pendingBooking(_id, updates)
//   Swal.fire({
//     title: "Success",
//     text: "Booking Pending",
//     icon: "success",
//     showConfirmationButton: false,
//     timer: 3000
//   })
// }

// const onClickApproveHandler = async (e) => {
//   e.preventDefault();
//   const updates = {
//     statusId
//   }
//   approveBooking(_id, updates)
//   Swal.fire({
//     title: "Success",
//     text: "Booking Approved",
//     icon: "success",
//     showConfirmationButton: false,
//     timer: 3000
//   })
// }

// const onClickDeclineHandler = async (e) => {
//   e.preventDefault();
//   const updates = {
//     statusId
//   }
//   declineBooking(_id, updates)
//   Swal.fire({
//     title: "Success",
//     text: "Booking Declined",
//     icon: "success",
//     showConfirmationButton: false,
//     timer: 3000
//   })
// }

const onClickPaymentHandler = async (e) => {
	e.preventDefault();
	console.log("BOOKING IS PAID", bookingData.isPaid)
		
	const updates = {
		isPaid: !bookingData.isPaid
	}

	payBooking(bookingData._id, updates)

	Swal.fire({
	  title: "Success",
	  text: "Booking Paid.",
	  icon: "success",
	  showConfirmationButton: false,
	  timer: 3000
	})
	setBookingData({
		...bookingData,
		isPaid: !bookingData.isPaid
	})
	// getBooking();
	console.log("BOOKING IS PAID TOGGLED", bookingData.isPaid)
}


const onClickCompletedHandler = async (e) => {
  e.preventDefault();
  const updates = {
   isCompleted: bookingData.isCompleted
  }

  if(bookingData.isCompleted == false) {
    completeBooking(bookingData._id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Completed",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
    console.log("IS COMPLETED TRUE", bookingData.isCompleted)
  } else if(bookingData.isCompleted == true) {
    ongoingBooking(bookingData._id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Ongoing",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
    console.log("IS COMPLETED FALSE", bookingData.isCompleted)
  }
  // const parkingUpdates = {
  //   isAvailable
  // }
  // enableParking(parkingId, parkingUpdates)
}

const onClickArchiveHandler = async (e) => {
  e.preventDefault();
  const updates = {
   isArchived: bookingData.isArchived
  }

  if(bookingData.isArchived == false) {
    archiveBooking(bookingData._id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Archived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  } else if(bookingData.isArchived == true) {
    unarchiveBooking(bookingData._id, updates)
     Swal.fire({
      title: "Success",
      text: "Booking Unarchived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
}

const onClickDeleteHandler = async (e) => {
  e.preventDefault();
  // setLoading(true)
  Swal.fire({
      title: "Success",
      text: "Booking Deleted",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  deleteBooking(bookingData._id)

}

let payment = ""
if(bookingData.isPaid) {
  payment = (
   <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickPaymentHandler(e) } > PAID </Button></td>
  )
} else {
  payment = (
   <td><Button className="mb-1 btn btn-sm btn-warning btn-block mx-1" onClick={ e => onClickPaymentHandler(e) }> UNPAID </Button></td>
  )
}

let complete = ""
if(bookingData.isCompleted == false) {
  complete = (
     <td><Button className="mb-1 btn btn-sm btn-warning btn-block mx-1" onClick={ e => onClickCompletedHandler(e) }> ON-GOING </Button></td>
  )
} else {
  complete = (
     <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickCompletedHandler(e) } >  COMPLETED </Button></td>
  )
}

let archival = ""
if(bookingData.isArchived == false) {
   archival = (
    <td><Button className="mb-1 btn btn-sm btn-success btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } > ACTIVE </Button></td>
  )
} else {
  archival =(
   <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } >  ARCHIVED </Button></td>
  )
}



	return (
		<Fragment>
			<Modal isOpen={modal} toggle={toggle}>
			<ModalHeader toggle={toggle}>VIEW BOOKING DETAILS</ModalHeader>
				<ModalBody>
					<Table>
						<thead>
							<tr>
								<th>DETAILS</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Booking ID:</td>
								<td>{bookingData._id}</td>
							</tr>
							<tr>
								<td>User:</td>
								<td>{bookingData.userId && bookingData.userId.firstName.toUpperCase()} {bookingData.userId && bookingData.userId.lastName.toUpperCase()}</td>
							</tr>
							<tr>
								<td>Date:</td>
								<td>{bookingData.startDate && moment(bookingData.startDate).format('MMMM DD, YYYY')}</td>
							</tr>
							<tr>
								<td>Slot:</td>
								<td>{bookingData.parkingId && bookingData.parkingId.name.toUpperCase()}</td>
							</tr>
							<tr>
								<td>Building:</td>
								<td>{bookingData.buildingId && bookingData.buildingId.name.toUpperCase()}</td>
							</tr>
							<tr>
								<td>Location:</td>
								<td>{bookingData.locationId && bookingData.locationId.name.toUpperCase()}</td>
							</tr>

						</tbody>
					</Table>
						<br/>
					<Table>
						<thead>
							<tr>
								<th className="text-left">STATUS</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								{payment}
							</tr>
							<tr>
								{complete}
							</tr>
							<tr>
								<td>
								<Button className="mb-1 btn btn-sm btn-success btn-block mx-1" disabled>PENDING</Button>
								</td>
							</tr>

							<tr>
								<td>
									<Button className="mb-1 btn btn-sm btn-block btn-warning ml-1" onClick={ e => onClickDeleteHandler(e) } ><i class="fas fa-trash-alt"></i>&nbsp;CANCEL BOOKING</Button> {/*CANCEL*/}
								</td>
							</tr>

						</tbody>
					</Table>



					<Form>
						<FormGroup className="mb-5">
							<h5>Are your sure you want to close booking details?</h5>
						</FormGroup>
						<div className=" d-flex justify-content-end">
							<Button className="btn btn-secondary mr-3" onClick={ () => toggle() }>CLOSE</Button>
						</div>
					</Form>
				</ModalBody>
			</Modal>
		</Fragment>
	)
}

export default BookingModalUser;