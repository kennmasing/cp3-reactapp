import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';

const LocationModal = ({modal, toggle, location, updateLocation, setLoading}) => {
	
	const [locationModal, setLocationModal] = useState({
		name: "",
		isArchived: ""
	})

	const {name} = locationModal;

	useEffect(() => {
		setLocationModal({
			name: location.name ? location.name : null,
			isArchived: location.isArchived ? location.isArchived : null// name: location.name ? location.name : null
		})
	}, [toggle])

	// let na;
	// if(isAvailable === null || isArchived === null) {
	// 	na = (
	// 		<option selected disabled>Select one</option>
	// 	)
	// }

	const onChangeHandler = (e) => {
		setLocationModal({
			...locationModal,
			[e.target.name] : e.target.value
		})
	}
	const onSubmitHandler = async (e) => {
		e.preventDefault();
		setLoading(true)
		const updates = {
			name
		}
		updateLocation(location._id, updates)
	}

	return (
		<Modal isOpen={modal} toggle={toggle}>
		<ModalHeader toggle={toggle}>UPDATE Location</ModalHeader>
			<ModalBody>
				<Form className="border rounded" onSubmit={e => onSubmitHandler(e)}>
					<FormGroup>
						<Label for="name">LOCATION NAME</Label>
						<Input type="text" name="name" id="name" value={name} onChange={e => onChangeHandler(e)}/>
					</FormGroup>					
					<Button className="btn btn-block bg-secondary">Save Changes</Button>
				</Form>
			</ModalBody>
		</Modal>
	)
}

export default LocationModal;