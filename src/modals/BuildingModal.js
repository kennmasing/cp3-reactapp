import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody } from 'reactstrap';
import axios from 'axios';

const BuildingModal = ({modal, toggle, building, updateBuilding}) => {
	
	const [buildingModal, setBuildingModal] = useState({
		name: ""
	})
	const {name} = buildingModal;
	useEffect(() => {
		setBuildingModal({
			// name: building.name ? building.name : null
		})
	}, [toggle])
	const onChangeHandler = (e) => {
		setBuildingModal({
			...buildingModal,
			[e.target.name] : e.target.value
		})
	}
	const onSubmitHandler = async (e) => {
		e.preventDefault();
		const updates = {
			name
		}
		// updateBuilding(building._id, updates)
	}

	return (
		<Modal isOpen={modal} toggle={toggle}>
		<ModalHeader toggle={toggle}>UPDATE Building</ModalHeader>
			<ModalBody>

			</ModalBody>
		</Modal>
	)
}

export default BuildingModal;