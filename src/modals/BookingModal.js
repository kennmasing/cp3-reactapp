import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';

const BookingModal = (props) => {
	
	const {
		modal,
		toggle,
		booking,
		updateBooking,
		parking,
		disableParking,
		enableParking,
		completeBooking,
		setLoading
	} = props

	const { _id } = parking
	const [bookingModal, setBookingModal] = useState({
		parkingId: "",
		isCompleted: "",
		isAvailable: parking.isAvailable
	})
	const { parkingId, isCompleted, isAvailable } = bookingModal;
	useEffect(() => {
		setBookingModal({
			// name: booking.name ? booking.name : null
			parkingId: booking.parkingId ? booking.parkingId : null,
			isCompleted: booking.isCompleted ? booking.isCompleted : null,
			isAvailable: parking.isAvailable ? parking.isAvailable : null
		})
	}, [toggle])
	const onChangeHandler = (e) => {
		setBookingModal({
			...bookingModal,
			[e.target.name] : e.target.value
		})
	}

	const onClickAvailableHandler = async (e) => {
	  e.preventDefault();
	  const updates = {
	    isAvailable
	  }
	  console.log("IS IT AVAILABLE", isAvailable)
	  if(isAvailable === false) {
		  enableParking(_id, updates)
		  Swal.fire({
		    title: "Success",
		    text: "Parking slot is now available.",
		    icon: "success",
		    showConfirmationButton: false,
		    timer: 3000
		  })
	  } else if(isAvailable === true) {
  		 disableParking(_id, updates)
		  Swal.fire({
		    title: "Success",
		    text: "Parking slot is disabled.",
		    icon: "success",
		    showConfirmationButton: false,
		    timer: 3000
		  })
	  }
	}
	const onSubmitHandler = async (e) => {
		e.preventDefault();
		const updates = {
			isCompleted
		}
		completeBooking(booking._id, updates)
		// updateBooking(booking._id, updates)
	}

	return (
		<Modal isOpen={modal} toggle={toggle}>
		<ModalHeader toggle={toggle}>UPDATE BOOKING</ModalHeader>
			<ModalBody>
				<Form>
					<FormGroup>
						<Label for="parkingId">PARKING NAME (ParkingId)</Label>
						<Input
							type="text"
							name="parkingId"
							id="parkingId"
							value={parkingId}
							disabled
						/>
					</FormGroup>
					<FormGroup>
						<Label for="isCompleted">Name</Label>
						<Input
							type="text"
							name="isCompleted"
							id="isCompleted"
							value={isCompleted}
							disabled
						/>
						<Button onClick={e => onClickAvailableHandler(e)} className="btn btn-primary">ENABLE</Button>
					</FormGroup>
					<Button className="btn btn-block bg-secondary">Save Changes</Button>
				</Form>
			</ModalBody>
		</Modal>
	)
}

export default BookingModal;