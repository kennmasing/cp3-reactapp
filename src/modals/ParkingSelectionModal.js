import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody } from 'reactstrap';
import axios from 'axios';

const ParkingSelectionModal = ({modal, toggle, Parking, updateParking}) => {
	
	const [parkingModal, setParkingModal] = useState({
		name: ""
	})
	const {name} = parkingModal;
	useEffect(() => {
		setParkingModal({
			// name: parking.name ? parking.name : null
		})
	}, [toggle])
	const onChangeHandler = (e) => {
		setParkingModal({
			...parkingModal,
			[e.target.name] : e.target.value
		})
	}
	const onSubmitHandler = async (e) => {
		e.preventDefault();
		const updates = {
			name
		}
		// updateParking(parking._id, updates)
	}

	return (
		<Modal isOpen={modal} toggle={toggle}>
		<ModalHeader toggle={toggle}>UPDATE Parking</ModalHeader>
			<ModalBody>

			</ModalBody>
		</Modal>
	)
}

export default ParkingSelectionModal;