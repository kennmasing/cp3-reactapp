import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody } from 'reactstrap';
import axios from 'axios';

const ParkingModal = ({modal, toggle, parking, updateParking}) => {
	
	// console.log("PARKING DATA", parking)

	const [parkingModal, setParkingModal] = useState({
		name: "",
		buildingId: "",
		isAvailable: "",
		isArchived: ""
	})
	const { name, buildingId, isAvailable, isArchived } = parkingModal;
	useEffect(() => {
		setParkingModal({
			name: parking.name ? parking.name : null,
			buildingId: parking.buildingId ? parking.buildingId.name : null,
			isAvailable: parking.isAvailable ? parking.isAvailable : null,
			isArchived: parking.isArchived ? parking.isArchived : null
		})
	}, [toggle])

	//IF TEAM ID IS NULL
	let na;
	if(isAvailable === null || isArchived === null) {
		na = (
			<option selected disabled>Select one</option>
		)
	}


	const onChangeHandler = (e) => {
		setParkingModal({
			...parkingModal,
			[e.target.name] : e.target.value
		})
	}

	const onSubmitHandler = async (e) => {
		e.preventDefault();
		
		const updates = {
			name
			// buildingId,
			// isAvailable,
			// isArchived
		}
		updateParking(parking._id, updates)
	}



	return (
		<Modal isOpen={modal} toggle={toggle}>
		<ModalHeader toggle={toggle}>UPDATE Parking</ModalHeader>
			<ModalBody>
				<Form className="border rounded" onSubmit={e => onSubmitHandler(e)}>
					<FormGroup>
						<Label for="name">SLOT NUMBER</Label>
						<Input type="text" name="name" id="name" value={name} onChange={e => onChangeHandler(e)}/>
					</FormGroup>					
					{/*<FormGroup>
						<Label for="buildingId">BUILDING NAME</Label>
						<Input type="text" name="buildingId" id="buildingId" value={buildingId} disabled />
					</FormGroup>
					<FormGroup>
						<Label for="isAvailable">AVAILABILITY</Label>
						<Input type="select" name="isAvailable" id="isAvailable" onChange={e => onChangeHandler(e)}>
							{ na }
							<option value={isAvailable} selected={ isAvailable == "true" ? true : false }>AVAILABLE</option>
							<option value={isAvailable} selected={ isAvailable == "false" ? true : false }>BOOKED</option>
						</Input>
					</FormGroup>
					<FormGroup>
						<Label for="isArchived">ARCHIVED</Label>
						<Input type="select" name="isArchived" id="isArchived" onChange={e => onChangeHandler(e)}>
							{ na }
							<option value={isArchived} selected={ isArchived == "true" ? true : false }>ACTIVE</option>
							<option value={isArchived} selected={ isArchived == "false" ? true : false }>ARCHIVED</option>
						</Input>
					</FormGroup>*/}
					<Button className="btn btn-block bg-secondary">Save Changes</Button>
				</Form>
			</ModalBody>
		</Modal>
	)
}

export default ParkingModal;