import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody } from 'reactstrap';
import axios from 'axios';

const UserModal = ({modal, toggle, User, updateUser}) => {
	
	const [userModal, setUserModal] = useState({
		name: ""
	})
	const {name} = userModal;
	useEffect(() => {
		setUserModal({
			// name: User.name ? User.name : null
		})
	}, [toggle])
	const onChangeHandler = (e) => {
		setUserModal({
			...userModal,
			[e.target.name] : e.target.value
		})
	}
	const onSubmitHandler = async (e) => {
		e.preventDefault();
		const updates = {
			name
		}
		// updateUser(User._id, updates)
	}

	return (
		<Modal isOpen={modal} toggle={toggle}>
		<ModalHeader toggle={toggle}>UPDATE User</ModalHeader>
			<ModalBody>

			</ModalBody>
		</Modal>
	)
}

export default UserModal;