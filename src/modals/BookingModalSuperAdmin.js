import React, { Fragment, useState, useEffect } from 'react';
import {
	Button,
	Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody,
	Table, 
} from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import moment from 'moment';

const BookingModalSuperAdmin = (props) => {
	
	const {
		modal,
		toggle,
		booking,
		index,
		roleId,
		token,
		pendingBooking,
		approveBooking,
		declineBooking,
		payBooking,
		unpayBooking,
		completeBooking,
		ongoingBooking,
		archiveBooking,
		unarchiveBooking,
		deleteBooking,
		enableParking,
		setLoading
	} = props

	const [ disabledBtn, setDisabledBtn ] = useState(false)
	const [ disabledDeclineBtn, setDisabledDeclineBtn ] = useState(true)

	const [bookingData, setBookingData] = useState({})

	useEffect(()=>{
		booking ? setBookingData(booking) : console.log("2 ++++")
		// setBookingData(booking)
		// console.log("BOOKING USER" ,booking.userId)
	}, [booking])

	console.log("BOOKING DATA", booking)	

	useEffect(() => {
		setBookingData(booking)
	}, [toggle])



// 	const onClickPendingHandler = async (e) => {
//   e.preventDefault();
//   const updates = {
//     statusId
//   }
//   pendingBooking(bookingData._id, updates)
//   Swal.fire({
//     title: "Success",
//     text: "Booking Pending",
//     icon: "success",
//     showConfirmationButton: false,
//     timer: 3000
//   })
// }

// const onClickApproveHandler = async (e) => {
//   e.preventDefault();
//   const updates = {
//     statusId
//   }
//   approveBooking(bookingData._id, updates)
//   Swal.fire({
//     title: "Success",
//     text: "Booking Approved",
//     icon: "success",
//     showConfirmationButton: false,
//     timer: 3000
//   })
// }

// const onClickDeclineHandler = async (e) => {
//   e.preventDefault();
//   const updates = {
//     statusId
//   }
//   declineBooking(bookingData._id, updates)
//   Swal.fire({
//     title: "Success",
//     text: "Booking Declined",
//     icon: "success",
//     showConfirmationButton: false,
//     timer: 3000
//   })
// }

const onClickPaymentHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isPaid: bookingData.isPaid
  }
  if(bookingData.isPaid == false) {
    payBooking(bookingData._id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Paid.",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })

  } else if(bookingData.isPaid == true) {
    unpayBooking(bookingData._id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Reversed",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
}

const onClickCompletedHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isCompleted: bookingData.isCompleted
  }

  if(bookingData.isCompleted == false) {
    completeBooking(bookingData._id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Completed",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  } else if(bookingData.isCompleted == true) {
    ongoingBooking(bookingData._id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Ongoing",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
  // const parkingUpdates = {
  //   isAvailable
  // }
  // enableParking(parkingId, parkingUpdates)
}

const onClickArchiveHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isArchived: bookingData.isArchived
  }

  if(bookingData.isArchived == false) {
    archiveBooking(bookingData._id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Archived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  } else if(bookingData.isArchived == true) {
    unarchiveBooking(bookingData._id, updates)
     Swal.fire({
      title: "Success",
      text: "Booking Unarchived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
}

const onClickDeleteHandler = async (e) => {
  e.preventDefault();
  // setLoading(true)
  Swal.fire({
      title: "Success",
      text: "Booking Deleted",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  deleteBooking(bookingData._id)

}

let payment = ""
if(bookingData.isPaid == false) {
  payment = (
   <td className="md-10 no-gutters"><Button className="mb-1 btn btn-sm btn-warning btn-block mx-1" onClick={ e => onClickPaymentHandler(e) } disabled={disabledBtn}> UNPAID </Button></td>
  )
} else if(bookingData.isPaid == true) {
  payment = (
   <td className="md-10 no-gutters"><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickPaymentHandler(e) } disabled> PAID </Button></td>
  )
}

let complete = ""
if(bookingData.isCompleted == false) {
  complete = (
     <td className="md-10 no-gutters"><Button className="mb-1 btn btn-sm btn-warning btn-block mx-1" onClick={ e => onClickCompletedHandler(e) } disabled={disabledBtn}> ON-GOING </Button></td>
  )
} else {
  complete = (
     <td className="md-10 no-gutters"><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickCompletedHandler(e) } disabled>  COMPLETED </Button></td>
  )
}

let archival = ""
if(bookingData.isArchived == false) {
   archival = (
    <td className="md-10 no-gutters"><Button className="mb-1 btn btn-sm btn-success btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } > ACTIVE </Button></td>
  )
} else {
  archival =(
   <td className="md-10 no-gutters"><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } disabled>  ARCHIVED </Button></td>
  )
}

let tableData = "";
if(roleId == 1) {
	tableData = (
		<Fragment>
			<Modal isOpen={modal} toggle={toggle}>
			<ModalHeader toggle={toggle}>VIEW BOOKING DETAILS</ModalHeader>
				<ModalBody>
					<Table>
						<thead>
							<tr>
								<th>DETAILS</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Booking ID:</td>
								<td>{bookingData._id}</td>
							</tr>
							<tr>
								<td>User:</td>
								<td>{bookingData.userId && bookingData.userId.firstName.toUpperCase()} {bookingData.userId && bookingData.userId.lastName.toUpperCase()}</td>
							</tr>
							<tr>
								<td>Date:</td>
								<td>{bookingData.startDate && moment(bookingData.startDate).format('MMMM DD, YYYY')}</td>
							</tr>
							<tr>
								<td>Slot:</td>
								<td>{bookingData.parkingId && bookingData.parkingId.name.toUpperCase()}</td>
							</tr>
							<tr>
								<td>Building:</td>
								<td>{bookingData.buildingId && bookingData.buildingId.name.toUpperCase()}</td>
							</tr>
							<tr>
								<td>Location:</td>
								<td>{bookingData.locationId && bookingData.locationId.name.toUpperCase()}</td>
							</tr>

						</tbody>
						<br/>
					</Table>

					<Table>
						<thead>
							<tr>
								<th className="text-left">STATUS</th>
							</tr>
						</thead>
					</Table>

					<Table>
						<tbody className="md-12">
							<tr className="md-12">
								{payment}
								<td className="text-right md-2 no-gutters">
									<Button className="mb-1 btn btn-sm btn-success ml-1" onClick={ e => onClickPaymentHandler(e) }><i class="fas fa-credit-card"></i></Button>
								</td>
							</tr>
							<tr className="md-12">
								{complete}
								<td className="text-right md-2 no-gutters">
									 <Button className="mb-1 btn btn-sm btn-info ml-1" onClick={ e => onClickCompletedHandler(e) }><i class="fas fa-check-square"></i></Button>
								</td>
							</tr>
							<tr className="md-12">
								<td className="md-10 no-gutters">
									<Button className="mb-1 btn btn-sm btn-success btn-block mx-1" disabled>PENDING</Button>
								</td>
								<td className="text-right md-2 no-gutters">
									<Button className="mb-1 btn btn-sm btn-info ml-1" ><i class="far fa-thumbs-up"></i></Button>
									<Button className="mb-1 btn btn-sm btn-warning ml-1" ><i class="fas fa-ban"></i></Button>
									<Button className="mb-1 btn btn-sm btn-secondary ml-1" ><i class="fas fa-wrench"></i></Button>
								</td>
							</tr>
							<tr className="md-12">
								{archival}
								<td className="text-right md-2 no-gutters">
									<Button className="mb-1 btn btn-sm btn-secondary ml-1" onClick={ e => onClickArchiveHandler(e) }><i class="fas fa-archive"></i></Button> 
								</td>
							</tr>
						</tbody>
					</Table>

					<Table>
						<tr>
							<td className="no-gutters">
								<Button className="mb-1 btn btn-sm btn-block btn-warning ml-1" onClick={ e => onClickDeleteHandler(e) } ><i class="fas fa-trash-alt"></i>&nbsp;CANCEL BOOKING</Button> {/*CANCEL*/}
							</td>
						</tr>
					</Table>
					<Form>
						<FormGroup className="mb-5">
							<h5>Are your sure you want to close booking details?</h5>
						</FormGroup>
						<div className=" d-flex justify-content-end">
							<Button className="btn btn-secondary mr-3" onClick={ () => toggle() }>CLOSE</Button>
						</div>
					</Form>
				</ModalBody>
			</Modal>
		</Fragment>
	)
} else if(roleId == 2) {
	tableData = (
		<Fragment>
			<Modal isOpen={modal} toggle={toggle}>
			<ModalHeader toggle={toggle}>VIEW BOOKING DETAILS</ModalHeader>
				<ModalBody>
					<Table>
						<thead>
							<tr>
								<th>DETAILS</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Booking ID:</td>
								<td>{bookingData._id}</td>
							</tr>
							<tr>
								<td>User:</td>
								<td></td>
							</tr>
							<tr>
								<td>Date:</td>
								<td>{moment(booking.startDate).format('MMMM DD, YYYY')}</td>
							</tr>
							<tr>
								<td>Slot:</td>
								<td></td>
							</tr>
							<tr>
								<td>Building:</td>
								<td>yyyyyy</td>
							</tr>
							<tr>
								<td>Location:</td>
								<td>yyyyyy</td>
							</tr>

						</tbody>


					</Table>
						<br/>
					<Table>
						<thead>
							<tr>
								<th className="text-left">STATUS</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								{payment}
							</tr>
							<tr>
								{complete}
							</tr>
							<tr>
								{archival}
							</tr>
						</tbody>

					</Table>
					<Table>
						<tbody className="col-12">
							<tr className="">
								<td className="md-7">
								<Button className="mb-1 btn btn-sm btn-success btn-block mx-1 col-12 no-gutters" disabled>PENDING</Button>
								</td>
								<td className="md-5 text-right">
									<Button className="mb-1 btn btn-sm btn-info ml-1 sm-2 col-3 no-gutters" ><i class="far fa-thumbs-up"></i></Button>
									<Button className="mb-1 btn btn-sm btn-warning ml-1 sm-2 col-3 no-gutters" ><i class="fas fa-ban"></i></Button>

								</td>
							</tr>
						</tbody>
					</Table>

					<Table>
						<tr>
							<td>
								<Button className="mb-1 btn btn-sm btn-block btn-warning ml-1" onClick={ e => onClickDeleteHandler(e) } ><i class="fas fa-trash-alt"></i>&nbsp;CANCEL BOOKING</Button> {/*CANCEL*/}
							</td>
						</tr>
					</Table>


					<Form>
						<FormGroup className="mb-5">
							<h5>Are your sure you want to close booking details?</h5>
						</FormGroup>
						<div className=" d-flex justify-content-end">
							<Button className="btn btn-secondary mr-3" onClick={ () => toggle() }>CLOSE</Button>
						</div>
					</Form>
				</ModalBody>
			</Modal>
		</Fragment>
	)
}

	return (
		<Fragment>
			{tableData}
		</Fragment>
	)
}

export default BookingModalSuperAdmin;