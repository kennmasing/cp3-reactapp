import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';

const PaymentBuildingModal = ({paymentModal, paymentToggle, booking, payBooking}) => {
	
	console.log("BOOKING ID", booking)
	const { _id, name, parkingId, isPaid, statusId, isCompleted, isArchived, createdAt } = booking
	console.log("BOOKING ID", _id)

	// const [bookingModal, setBookingModal] = useState({
	// 	_id: ""
	// })

	// const { _id } = bookingModal;

	// useEffect(() => {
	// 	setBookingModal({
	// 		...bookingModal,
	// 		_id:  booking._id ? booking._id : null
	// 	})
	// }, [toggle])
	
	// const onClickPaymentHandler = async (e) => {
	//   e.preventDefault();
	//   Swal.fire({
 //          title: "Success",
 //          text: "Booking Paid",
 //          icon: "success",
 //          showConfirmationButton: false,
 //          timer: 3000
 //        })
	//   payBooking(_id)
	// }

	return (
		<Modal isOpen={paymentModal} toggle={paymentToggle}>
		<ModalHeader toggle={paymentToggle}>Payment</ModalHeader>
			<ModalBody>
				<Form>
					<FormGroup className="mb-5">
						<h5>Are your sure you want to pay booking?</h5>
					</FormGroup>
					<div className=" d-flex justify-content-end">
						<Button className="btn btn-secondary mr-3" onClick={ () => paymentToggle() }>Cancel</Button>
						<Button className="btn btn-warning" onClick={payBooking(_id)}>Pay</Button>
					</div>
				</Form>
			</ModalBody>
		</Modal>
	)
}

export default PaymentBuildingModal;