import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Parallax } from 'react-scroll-parallax';

import bikeparking from '../images/styling/bikeparking.png';
import HeaderCardMobile from '../cards/HeaderCardMobile';

const ParallaxImage = () => (
    <Parallax className="custom-class mt-n4" y={[-20, 20]} tagOuter="figure">
        <div className="App home01">
        	<HeaderCardMobile />
        	
        </div>
        	}
    </Parallax>
);

export default ParallaxImage;