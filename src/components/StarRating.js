import React, { useState, useEffect } from 'react'
import { FaStar } from "react-icons/fa"

const StarRating = ({updateRating, ratingValue}) => {
	console.log("RATING VALUE", ratingValue)

	const [rating, setRating] = useState(null);
	const [hover, setHover] = useState(null);

	useEffect(() => {
		setRating(ratingValue)
	}, [ratingValue])

	const onClickHandler = (e) => {
		setRating(e.target.value)
		// onClick={ e => setRating(ratingValue) }
		updateRating()
	}

	return (
		<div>
			{[...Array(5)].map((star, i)=> {
				const ratingValue = i + 1;
				return (
					<label className="text-center">
						<input
							type="radio"
							name="rating"
							value={ratingValue}
						/>
						<FaStar
							className="star"
							color={ratingValue <= (hover || rating) ? "#ffc107" : "#e4e5e9"}
							size={75}
							onMouseEnter={ e => setHover(ratingValue) }
							onMouseLeave={ e => setHover(null)}
						/>
					</label>
				) 
			})}
			{/*<h4 className="text-center">The rating is {rating}.</h4>*/}
		</div>
	)
}

export default StarRating;