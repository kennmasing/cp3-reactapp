//(1) IMPORT DEPENDENCIES
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

//(2) DEFINE THE ROOT
const root = document.querySelector('#root');

//(3) CREATE A COMPONENT
const pageComponent = <App/>

//(4) RENDER THE COMPONENT
ReactDOM.render(pageComponent, root)