import React from 'react';
import { Table } from 'reactstrap';

import BuildingRow from '../rows/BuildingRow';

const BuildingTable = (props) => {

  let row;

  if(!props.buildings) {
    row = (
      <tr>
        <td>
          <em>There are currently no Buildings.</em>
        </td>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.buildings.map(building => {
        return <BuildingRow
          building={building}
          key={building._id}
          index={++i}
          toggle={props.toggle}

          archiveBuilding={props.archiveBuilding}
          unarchiveBuilding={props.unarchiveBuilding}

          deleteBuilding={props.deleteBuilding}

          setLoading={props.setLoading}
        />
      })
    )
  }

  return (
    <Table responsive hover light borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th className="text-center">#</th>
          <th className="text-left">NAME</th>
          <th className="text-center">DATE CREATED</th>
          <th className="text-center">LOCATION</th>
          <th className="text-center">STATUS</th>
          <th className="text-center">ACTIONS</th>
        </tr>
      </thead>
      <tbody>
        { row }
      </tbody>
    </Table>
  );
}

export default BuildingTable;