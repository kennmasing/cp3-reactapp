import React, { Fragment } from 'react';
import { Table } from 'reactstrap';

import BookingRow from '../rows/BookingRow';

const BookingTable = (props) => {

  let row;

  if(!props.bookings) {
    row = (
      <tr>
        <td>
          <em>There are currently no bookings.</em>
        </td>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.bookings.map(booking => {
        console.log("MAPPED BOOKING TYPE", typeof booking)
        console.log("MAPPED BOOKING", booking)
        return <BookingRow
          token={props.token}
          roleId={props.roleId}
          booking={booking}
          key={booking._id}
          index={++i}
          toggle={props.toggle}

          pendingBooking={props.pendingBooking}
          approveBooking={props.approveBooking}
          declineBooking={props.declineBooking}

          payBooking={props.payBooking}
          unpayBooking={props.unpayBooking}

          completeBooking={props.completeBooking}
          ongoingBooking={props.ongoingBooking}

          archiveBooking={props.archiveBooking}
          unarchiveBooking={props.unarchiveBooking}

          deleteBooking={props.deleteBooking}
          enableParking={props.enableParking}

          setLoading={props.setLoading}
        />
      })
    )
  }

let tableHeaders = "";
if(props.roleId == 1) {
   tableHeaders = (
    <Fragment>
      <th className="text-center">#</th>
      <th className="text-center">User & Booking ID</th>
      <th className="text-right">Creation</th>
      <th className="text-left">& Reservation Date</th>
      <th className="text-center">Parking Slot</th>
      <th className="text-center">Payment Status</th>
      <th className="text-center">Completion Status</th>
      <th className="text-center">Approval Status</th>
      <th className="text-center">Archival Status</th>
      <th className="text-center">Admin Actions</th>
      <th className="text-center">Super Admin Actions</th>
    </Fragment>
  )
} else if(props.roleId == 2) {
   tableHeaders = (
    <Fragment>
      <th className="text-center">#</th>
      <th className="text-center">User & Booking ID</th>
      <th className="text-right">Creation</th>
      <th className="text-left">& Reservation Date</th>
      <th className="text-center">Parking Slot</th>
      {/*<th className="text-center">Payment Status</th>
      <th className="text-center">Completion Status</th>
      <th className="text-center">Archival Status</th>*/}
      <th className="text-center">Approval Status</th>
      <th className="text-center">Admin Actions</th>
    </Fragment>
  )
} 

  return (
    <Table responsive hover light borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          { tableHeaders }
        </tr>
      </thead>
      <tbody>
        { row }
      </tbody>
    </Table>
  );
}

export default BookingTable;