import React from 'react';
import { Table } from 'reactstrap';

import LocationRow from '../rows/LocationRow';

const LocationTable = (props) => {

  let row;

  if(!props.locations) {
    row = (
      <tr>
        <td>
          <em>No locations.</em>
        </td>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.locations.map(location => {
        return <LocationRow
          location={location}
          key={location._id}
          index={++i}
          toggle={props.toggle}
          
          archiveLocation={props.archiveLocation}
          unarchiveLocation={props.unarchiveLocation}

          deleteLocation={props.deleteLocation}

          setLoading={props.setLoading}
        />
      })
    )
  }

  return (
    <Table responsive hover light borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th className="text-center">#</th>
          <th className="text-left">NAME</th>
          <th className="text-center">DATE CREATED</th>
          <th className="text-center">STATUS</th>
          <th className="text-center">ACTIONS</th>
        </tr>
      </thead>
      <tbody>
        { row }
      </tbody>
    </Table>
  );
}

export default LocationTable;