import React, { Fragment } from 'react';
import { Table } from 'reactstrap';

import UserBookingRowMobile from '../rows/UserBookingRowMobile';

const UserBookingTableMobile = (props) => {

  let row;

  if(!props.bookings) {
    row = (
      <tr>
        <td>
          <em>There are currently no bookings.</em>
        </td>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.bookings.map(booking => {
        return <UserBookingRowMobile
          token={props.token}
          roleId={props.roleId}
          booking={booking}
          key={booking._id}
          index={++i}
          toggle={props.toggle}

          pendingBooking={props.pendingBooking}
          approveBooking={props.approveBooking}
          declineBooking={props.declineBooking}

          payBooking={props.payBooking}
          unpayBooking={props.unpayBooking}

          completeBooking={props.completeBooking}
          ongoingBooking={props.ongoingBooking}

          archiveBooking={props.archiveBooking}
          unarchiveBooking={props.unarchiveBooking}

          deleteBooking={props.deleteBooking}
          enableParking={props.enableParking}

          setLoading={props.setLoading}
        />
      })
    )
  }

  return (
    <Table responsive hover light borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
           <Fragment>
      <th className="text-center">#</th>
      <th className="text-center">Details</th>
      <th className="text-center">Status/Actions</th>
    </Fragment>
        </tr>
      </thead>
      <tbody>
        { row }
      </tbody>
    </Table>
  );
}

export default UserBookingTableMobile;