import React from 'react';
import { Table } from 'reactstrap';

import ParkingRow from '../rows/ParkingRow';

const ParkingTable = (props) => {

  let row;

  if(!props.parkings) {
    row = (
      <tr>
        <td>
          <em>There are currently no Parkings.</em>
        </td>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.parkings.map(parking => {
        return <ParkingRow
          parking={parking}
          key={parking._id}
          index={++i}
          toggle={props.toggle}

          enableParking={props.enableParking}
          disableParking={props.disableParking}

          archiveParking={props.archiveParking}
          unarchiveParking={props.unarchiveParking}

          deleteParking={props.deleteParking}
        />
      })
    )
  }

  return (
    <Table responsive hover light borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th className="text-center">#</th>
          <th className="text-left">NAME</th>
          <th className="text-center">DATE CREATED</th>
          <th className="text-center">BUILDING NAME</th>
          <th className="text-center">AVAILABILITY</th>
          <th className="text-center">ARCHIVAL STATUS</th>
          <th className="text-center">ACTIONS</th>
        </tr>
      </thead>
      <tbody>
        { row }
      </tbody>
    </Table>
  );
}

export default ParkingTable;