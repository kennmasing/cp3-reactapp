import React from 'react';
import { Table } from 'reactstrap';

import UserRow from '../rows/UserRow';

const UserTable = (props) => {

  let row;

  if(!props.users) {
    row = (
      <tr>
        <td>
          <em>There are currently no Users.</em>
        </td>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.users.map(user => {
        return <UserRow
          user={user}
          key={user._id}
          index={++i}
          toggle={props.toggle}
          archiveUser={props.archiveUser}
          unarchiveUser={props.unarchiveUser}
          deleteUser={props.deleteUser}
        />
      })
    )
  }

  return (
    <Table responsive hover light borderless size="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th className="text-center">#</th>
          <th className="text-center">LAST NAME</th>
          <th className="text-center">FIRST NAME</th>
          <th className="text-center">USERNAME</th>
          <th className="text-center">EMAIL</th>
          <th className="text-center">ROLE</th>
          <th className="text-center">STATUS</th>
          <th className="text-center">ACTIONS</th>
        </tr>
      </thead>
      <tbody>
        { row }
      </tbody>
    </Table>
  );
}

export default UserTable;