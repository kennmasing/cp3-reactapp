import React, { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import axios from 'axios';
import { URL } from '../config';

import logo from '../images/styling/parqerlogo07.png';

const AppNavbar = (props) => {

  const {
    token,
    _id,
    currentUser,
    roleId
  } = props

  const [userData, setUserData] = useState({
    user: {}
  })
  const { user } = userData;
  const getUser = async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
      const res = await axios.get(`${URL}/users/${localStorage._id}`, config)
      console.log("USER RESPONSE", res)
      setUserData({
        ...userData,
        user: res.data
      })
      console.log("USER DATA", user)
    } catch(e) {
      console.log(e.response)
    }
  }
  useEffect(() => {
    getUser()
  }, [setUserData])

  const { firstName, lastName } = user;

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  let leftLinks = "";
  if(parseInt(roleId) === 1) {
    leftLinks = (
      <Fragment>
        <NavItem>
          <Link to="/bookings" className="nav-link">Bookings</Link>
        </NavItem>
        <UncontrolledDropdown nav inNavbar>
          <DropdownToggle nav caret>
            Resources
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem>
              <Link to="/locations" className="nav-link">Locations</Link>
            </DropdownItem>
            <DropdownItem>
              <Link to="/buildings" className="nav-link">Buildings</Link>
            </DropdownItem>
            <DropdownItem>
              <Link to="/parkings" className="nav-link">Parkings</Link>
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
              Reset
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <NavItem>
           <Link to="/users" className="nav-link">Users</Link>
        </NavItem>
      </Fragment>
    )
  } else if(parseInt(roleId) === 2) {
    leftLinks = (
      <Fragment>
        <NavItem>
          <Link to="/bookings" className="nav-link">Bookings</Link>
        </NavItem>
        <NavItem>
           <Link to="/users" className="nav-link">Users</Link>
        </NavItem>
      </Fragment>
    )
  } else if(parseInt(roleId) === 3) {
    leftLinks = (
      <Fragment>
        <NavItem>
          <Link to="/mybookings" className="nav-link">My Bookings</Link>
        </NavItem>
         <NavItem>
          <Link to="/home" className="nav-link">Book Now</Link>
        </NavItem> 
      </Fragment>
    )
  }

  let rightLinks = "";
  if(!token) {

  } else {
    rightLinks = (
     <Fragment>
         <Link to="/me" className="btn btn-outline-light">Welcome, {currentUser.charAt(0).toUpperCase()}{currentUser.slice(1)} </Link>
         <Link to="/logout" className="ml-3 btn btn-outline-light">Logout</Link>
      </Fragment>
    )
  }

  return (
    <div className="">
      <Navbar color="primary" dark expand="md" className="navbar-height mb-5">
        <NavbarBrand className="font-weight-bold ml-3 d-flex mb-n1" href="/">
          <img src={logo} className="parqer-logo mr-3"/>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              { leftLinks }
     
            </Nav>
            <div className="mr-3">
              { rightLinks }
            </div>

          </Collapse>
      </Navbar>
    </div>
  )

}

export default AppNavbar;
