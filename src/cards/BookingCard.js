import React, { Fragment } from 'react';
import { Table, Card } from 'reactstrap';

import BookingCardBody from '../cardbodies/BookingCardBody';

const BookingCard = (props) => {

  let card;

  if(!props.bookings) {
    card = (
      <tr>
        <td>
          <em>There are currently no bookings.</em>
        </td>
      </tr>
    )
  } else {
    let i = 0;
    card = (
        props.bookings.map(booking => {
        return <BookingCardBody
          token={props.token}
          roleId={props.roleId}
          booking={booking}
          key={booking._id}
          index={++i}
          toggle={props.toggle}

          pendingBooking={props.pendingBooking}
          approveBooking={props.approveBooking}
          declineBooking={props.declineBooking}

          payBooking={props.payBooking}
          unpayBooking={props.unpayBooking}

          completeBooking={props.completeBooking}
          ongoingBooking={props.ongoingBooking}

          archiveBooking={props.archiveBooking}
          unarchiveBooking={props.unarchiveBooking}

          deleteBooking={props.deleteBooking}
          enableParking={props.enableParking}

          setLoading={props.setLoading}
        />
      })
    )
  }

// let tableHeaders = "";
// if(props.roleId == 1) {
//    tableHeaders = (
//     <Fragment>
//     </Fragment>
//   )

// } else if(props.roleId == 2) {
//    tableHeaders = (
//     <Fragment>
//     </Fragment>
//   )
// } 

  {/*<Card className="col-5 container-fluid d-flex justify-content-center align-items-center my-1 mx-1 card-vh">
    {card}
  </Card>*/}

  return (
    <Fragment>
        {card}
    </Fragment>
  );
}

export default BookingCard;