import React, { Fragment } from 'react';
import { Card, CardBody, CardTitle, Container, Col, Row } from 'reactstrap';

import ParkingSelectionBody from '../cardbodies/ParkingSelectionBody';

const ParkingSelectionCard = (props) => {
	console.log("PARKING CARD TOKEN", props.token)

	return (
		<Fragment>
				{/* cardbody */}
				<ParkingSelectionBody token={props.token}/>
		</Fragment>
		)
}

export default ParkingSelectionCard;