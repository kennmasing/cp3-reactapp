import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col, Button, Form, FormGroup,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
// import StarRating from '../components/StarRating';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';

import UserProfileForm from '../forms/UserProfileForm';
import UserTable from '../tables/UserTable';
import UserModal  from '../modals/UserModal';

const UserProfilePage = (props) => {
	console.log("PROPS TOKEN", props.token)
	// const [currentUserData, setCurrentUserData] = useState({
	// 	token: props.token,
	// 	currentUser: ""
	// })
	const [currentUser, setCurrentUser] = useState({})
	// const { token, currentUser } = currentUserData;
	const config = {
		headers: {
			Authorization: `Bearer ${props.token}`
		}
	}
  	// const url = "http://localhost:8000"
	const getCurrentUser = async () => {
		try {
			const res = await axios.get(`${URL}/users/me`, config)
			console.log("Users RESPONSE", res)
			return setCurrentUser(res.data)
			console.log("Users DATA", currentUser)
		} catch(e) {
			console.log(e.response)
	      	//SWAL
		      Swal.fire({
		        title: "Error",
		        text: e.response.data.message,
		        icon: "error",
		        showConfirmationButton: false,
		        timer: 3000
		      })
		}
	}
	useEffect(() => {
		getCurrentUser()
	}, [setCurrentUser])
	//MODAL
	const [modalData,  setModalData] = useState({
		modal: false,
		user: {}
	})
	const { modal, user } = modalData;
	//TOGGLE FUNCTION
	const toggle = async (id) => {
		console.log(id);
		alert(id);
		try {	
			if(typeof id === "string") {
				const res = await axios.get(`${URL}/users/${id}`)

				return setModalData({
					modal: !modal,
					user: res.data
				})
				console.log("GET User", res)
			}
			setModalData({
				...modalData,
				modal: !modal
			})
		} catch(e) {
			console.log(e)
		}
	}
	//USE EFFECT
	useEffect(() => {

	}, [toggle])
	//UPDATE RATE
	const updateUser = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/users/${id}`, body, config)
			setModalData({
				...modalData,
				modal: !modal
			})
			getCurrentUser()
			// SWAL
			Swal.fire({
              title: "Success",
              text: "Updated current user profile!",
              icon: "success",
              showConfirmationButton: false,
              timer: 3000
            })
		} catch(e) {
			console.log(e)
		}
	}
	
	return (
		<Fragment>
			<h1 className="container-fluid text-center">PROFILE PAGE</h1>
			<div className="d-flex justify-content-center align-items-start login-vh col-10 offset-md-1">
				<Col md="10" className="ml-5">
					<UserProfileForm token={props.token} currentUser={currentUser} updateUser={updateUser}/>
				</Col>
				{/*<Col md="8" className="mr-5">
					<UserTable toggle={toggle} users={users}/>
					<UserModal modal={modal} toggle={toggle} user={user} updateUser={updateUser}/>
				</Col>*/}
			</div>
		</Fragment>
	)
}

export default UserProfilePage;