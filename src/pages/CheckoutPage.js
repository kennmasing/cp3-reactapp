import React, { Fragment, useState, useEffect } from 'react';
import { Container, Row, Col,
	Table
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import { URL } from '../config';
import axios from 'axios';

const CheckoutPage = (props) => {

	const [bookingsData, setBookingsData] = useState({
		token: props.token,
		bookings: []
	})
	const { bookings } = bookingsData;
	const config = {
		headers: {
			Authorization: `Bearer ${localStorage.token}`
		}
	}
	const getBookings = async () => {
		try {
			const res = await axios.get(`${URL}/bookings`, config)
			console.log("bookings RESPONSE", res)
			return setBookingsData({
				...bookingsData,
				bookings: res.data})
			console.log("bookings DATA", bookings)
		} catch(e) {
			console.log(e.response)
		}
	}
	useEffect(() => {
		getBookings()
	}, [setBookingsData])


let row = ""
row = (
	// bookings.map(booking => {
	// 	<td>{booking}</td>
	// })

	<div>
	{/* item is object with user's name and its other details on it */}
		{bookings.map((booking, index) => {
		return <div key={index}>--note the name property is primitive--->{booking.statusId.name}</div>;
	})}
	</div>
)



	return (
		<Fragment>
			<h1>CHECKOUT PAGE</h1>
			<Table responsive hover light borderless size="sm" className="p-4 rounded">
				<thead>
					<tr>
						<th>BOOKING</th>
					</tr>
				</thead>
				<tbody>
					{ row }
				</tbody>
			</Table>
		</Fragment>
	)
}

export default CheckoutPage;