import React, { Fragment, useState, useEffect } from 'react';
import { Col, Button } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config'

import LocationForm from '../forms/LocationForm';
import LocationTable from '../tables/LocationTable';
import LocationModal from '../modals/LocationModal';

const LocationPage = (props) => {

	console.log("LOCATION PROPS TOKEN", props.token)
	const [locationsData, setLocationsData] = useState({
		token: props.token,
		locations: []
	})
	const { token, locations } = locationsData;
	const config = {
		headers: {
			'Authorization' : `Bearer ${props.token}`
		}
	}
  	// const url = "http://localhost:8000"
  	const getLocations = async (query = "") => {
  		try {
  			const res = await axios.get(`${URL}/locations${query}`, config)
  			console.log("LOCATIONS RESPONSE", res)
  			return setLocationsData({
  				...locationsData,
  				locations: res.data
  			})
  			console.log("LOCATIONS DATA", locations)
  		} catch(e) {
  			console.log(e.response)
	      	//SWAL
		      Swal.fire({
		        title: "Error",
		        text: e.response.data.message,
		        icon: "error",
		        showConfirmationButton: false,
		        timer: 3000
		      })
  		}
  	}
  	useEffect(() => {
  		getLocations()
  	}, [setLocationsData])
	//UPDATE Location: MODAL
	const [modalData, setModalData] = useState({
		modal: false,
		location: {}
	})
	const { modal, location } = modalData;
	const toggle = async (id) => {
		console.log(id);
		// alert(id);
		try {
		  if(typeof id === "string") {
		    const res = await axios.get(`${URL}/locations/${id}`, config)
		    console.log(res)
		    return setModalData({
		      modal: !modal,
		      location: res.data
		    })
		  }
		  setModalData({
		    ...modalData,
		    modal: !modal
		  })
		} catch(e) {
		  console.log(e)
		}
	}
	useEffect(() => {

	}, [toggle])

	const updateLocation = async (id, updates) => {
		console.log("UPDATE LOCATION FROM MODAL", id, updates)
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/locations/${id}/update`, body, config)
			return setModalData({
				...modalData,
				modal: !modal
			})
		getLocations()
		props.setLoading(false)
		Swal.fire({
              title: "Success",
              text: "Successfully updated new location!",
              icon: "success",
              showConfirmationButton: false,
              timer: 3000
        })
		} catch(e) {
			console.log(e)
			props.setLoading(false)
		}
	}	
   	const archiveLocation = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/locations/${id}/archive`, body, config)
			getLocations()
		} catch(e) {
			console.log(e)
		}
	}
	const unarchiveLocation = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/locations/${id}/unarchive`, body, config)
			getLocations()
		} catch(e) {
			console.log(e)
		}
	}
	const deleteLocation = async (id) => {
	    try {
	      	const res = await axios.delete(`${URL}/locations/${id}`, config)
	      	// props.setLoading(false)
	      	getLocations()
	    } catch(e) {
	      	console.log(e)
	      	// props.setLoading(false)
	    }
	}

	return (
		<Fragment>	
			<h1 className="container-fluid text-center page-hdr">LOCATION PAGE</h1>
			<div className="d-flex justify-content-center align-items-start col-10 offset-md-1">
				<Col md="4" className="">
					<LocationForm setLoading={props.setLoading}/>
				</Col>
				<Col md="8" className="">
					<div className="d-flex justify-content-end">	
						<div >
							<Button className="btn-sm border mr-1" onClick={ ()=> getLocations() }>Get All</Button>
							<Button className="btn-sm border mr-1" onClick={ ()=> getLocations("?isArchived=false") }>Get Active</Button>
							<Button className="btn-sm border mr-1" onClick={ ()=> getLocations("?isArchived=true") }>Get Archived</Button>
						</div>
					</div>
					<hr/>
					<LocationTable
						toggle={toggle}
						locations={locations}

						updateLocation={updateLocation}

						archiveLocation={archiveLocation}
						unarchiveLocation={unarchiveLocation}

						deleteLocation={deleteLocation}
					/>
					<LocationModal
						modal={modal}
						toggle={toggle}
						location={location}
						updateLocation={updateLocation}

						setLoading={props.setLoading}
					/>
				</Col>
			</div>
		</Fragment>
	)
}

export default LocationPage;