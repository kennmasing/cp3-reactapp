import React, { Fragment, useState, useEffect } from 'react';
import { Col } from 'reactstrap';
import { Link } from 'react-router-dom';

//NESTING PAGES
import BookingForm from '../forms/BookingForm';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config'


import logo from '../images/styling/parqerlogo09.png';
import logo2 from '../images/styling/parqerlogo12.png';


const LandingPage = (props) => {
console.log("TOKEN", props.token)

//----------------USER LANDING PAGE------------------

//------------------LOCATIONS---------------------
//USE STATE
// console.log("LOCATION PROPS TOKEN", props.token)
// 	const [locationsData, setLocationsData] = useState({
// 		locations: []
// 	})
// 	const { locations } = locationsData;
//   	// const url = "http://localhost:8000"
//   	const getLocations = async () => {
//   		try {
//   			const res = await axios.get(`${URL}/locations`)
//   			console.log("LOCATIONS RESPONSE", res)
//   			return setLocationsData({
//   				...locationsData,
//   				locations: res.data
//   			})
//   			console.log("LOCATIONS DATA", locations)
//   		} catch(e) {
//   			console.log(e.response)
//   		}
//   	}
//   	useEffect(() => {
//   		getLocations()
//   	}, [setLocationsData])
 	
//   //------------------BUILDINGS---------------------
//   console.log("BUILDING PROPS TOKEN", props.token)
// 	const [buildingsData, setBuildingsData] = useState({
// 		buildings: []
// 	})
// 	const { buildings } = buildingsData;
//   	const getBuildings = async (id) => {
//   		try {
//   			const res = await axios.get(`${URL}/buildings/${id}`)
//   			console.log("BUILDINGS RESPONSE", res)
//   			return setBuildingsData({
//   				...buildingsData,
//   				buildings: res.data
//   			})
//   			console.log("BUILDINGS DATA", buildings)
//   		} catch(e) {
//   			console.log(e.response)
//   		}
//   	}
//   	useEffect(() => {
//   		getBuildings()
//   	}, [setBuildingsData]) 

  	
//   //------------------PARKINGS---------------------
//   console.log("PARKING PROPS TOKEN", props.token)
// 	const [parkingsData, setParkingsData] = useState({
// 		parkings: []
// 	})
// 	const { token, parkings } = parkingsData;
//   	const getParkings = async (id) => {
//   		try {
//   			const res = await axios.get(`${URL}/parkings/${id}`)
//   			console.log("PARKINGS RESPONSE", res)
//   			return setParkingsData({
//   				...parkingsData,
//   				parkings: res.data
//   			})
//   			console.log("PARKING DATA", parkings)
//   		} catch(e) {
//   			console.log(e.response)
//   		}
//   	}
//   	useEffect(() => {
//   		getParkings()
//   	}, [setParkingsData])

	return (
    <Fragment>
      <div class="Container App landing landing-desktop"> 
        <div className="mt-n5">
          <div className="d-flex justify-content-center">
                <img src={logo2} className="parqer-logo-landing-text" />
          </div>
           <div className="">
                <h1 className="text-landing-desktop pre-margin text-center"> Reserve a slot. Commute to work. </h1>
          </div>
          <div className="Container mt-n5 mb-4 d-flex justify-content-center align-items-center">
            <div class="menu">
              <div class="item item3">
                <div class="shadow"></div>
                <div class="left"></div>
                <div class="right"></div>
                <div class="top d-flex justify-content-center align-items-center">
                  <Link to="/login"><img src={logo} className="parqer-logo-landing" /></Link>
                </div>
              </div>      
            </div>
          </div>
        </div>
      </div>
      <div class="Container App landing landing-mobile"> 
        <div className="mt-n5">
          <div className="d-flex justify-content-center">
                <img src={logo2} className="parqer-logo-landing-text" />
          </div>
           <div className="">
                <h1 className="text-landing-desktop pre-margin text-center"> Reserve a slot. Commute to work. </h1>
          </div>
          <div className="Container mt-n5 mb-4 d-flex justify-content-center align-items-center">
            <div class="menu">
              <div class="item item3">
                <div class="shadow"></div>
                <div class="left"></div>
                <div class="right"></div>
                <div class="top d-flex justify-content-center align-items-center">
                  <Link to="/login"><img src={logo} className="parqer-logo-landing" /></Link>
                </div>
              </div>      
            </div>
          </div>
        </div>
      </div>

    </Fragment>
	)
}

export default LandingPage;