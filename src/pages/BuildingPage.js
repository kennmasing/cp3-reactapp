import React, { Fragment, useState, useEffect } from 'react';
import { Col, Button } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config'

import BuildingForm from '../forms/BuildingForm';
import BuildingTable from '../tables/BuildingTable';
import BuildingModal from '../modals/BuildingModal';

const BuildingPage = (props) => {

	//------------------LOCATIONS---------------------
	//USE STATE
	console.log("LOCATION PROPS TOKEN", props.token)
	const [locationsData, setLocationsData] = useState({
		locations: []
	})
	const { locations } = locationsData;
  	// const url = "http://localhost:8000"
  	const getLocations = async () => {
  		try {
  			const res = await axios.get(`${URL}/locations`)
  			console.log("LOCATIONS RESPONSE", res)
  			return setLocationsData({
  				...locationsData,
  				locations: res.data
  			})
  			console.log("LOCATIONS DATA", locations)
  		} catch(e) {
  			console.log(e.response)
  		}
  	}
  	useEffect(() => {
  		getLocations()
  	}, [])

  //------------------BUILDINGS---------------------
  	console.log("BUILDING PROPS TOKEN", props.token)
	const [buildingsData, setBuildingsData] = useState({
		token: props.token,
		buildings: []
	})
	const { token, buildings } = buildingsData;
	const config = {
		headers: {
			'Authorization' : `Bearer ${props.token}`
		}
	}
  	// const url = "http://localhost:8000"
  	const getBuildings = async (locId = "", query = "") => {
  		try {
  			const res = await axios.get(`${URL}/buildings${locId}${query}`, config)
  			console.log("BUILDINGS RESPONSE", res)
  			return setBuildingsData({
  				...buildingsData,
  				buildings: res.data
  			})
  			console.log("BUILDINGS DATA", buildings)
  		} catch(e) {
  			console.log(e.response)
	      	//SWAL
		      // Swal.fire({
		      //   title: "Error",
		      //   text: e.response.data.message,
		      //   icon: "error",
		      //   showConfirmationButton: false,
		      //   timer: 3000
		      // })
  		}
  	}

	//UPDATE Building: MODAL
	const [modalData, setModalData] = useState({
		modal: false,
		building: {}
	})
	const { modal, building } = modalData;
	const toggle = async (id) => {
		console.log(id);
		// alert(id);
		try {
		  if(typeof id === "string") {
		    const res = await axios.get(`${URL}/buildings/${id}`, config)
		    console.log(res)
		    return setModalData({
		      modal: !modal,
		      building: res.data
		    })
		  }
		  setModalData({
		    ...modalData,
		    modal: !modal
		  })
		} catch(e) {
		  console.log(e)
		}
	}
	useEffect(() => {

	}, [toggle])
	const updateBuilding = async (id, updates) => {
		console.log("UPDATE Building", id, updates)
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/buildings/${id}`, body, config)
			return setModalData({
				...modalData,
				modal: !modal
			})
		// getBuildings()
		//SWAL
		} catch(e) {
			console.log(e)
		}
	}


	//----------------ADMIN ACTIONS-------------------
	const archiveBuilding = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/buildings/${id}/archive`, body, config)
			
			getBuildings()
		} catch(e) {
			console.log(e)
		}
	}
	const unarchiveBuilding = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/buildings/${id}/unarchive`, body, config)
			
			getBuildings()
		} catch(e) {
			console.log(e)
		}
	}
	const deleteBuilding = async (id) => {
	    try {
	      	const res = await axios.delete(`${URL}/buildings/${id}/delete`, config)
	      	// props.setLoading(false)
	      	getBuildings()
	    } catch(e) {
	      	console.log(e)
	      	// props.setLoading(false)
	    }
	}

	return (
		<Fragment>
			<h1 className="container-fluid text-center page-hdr">BUILDING PAGE</h1>
			<div className="d-flex justify-content-center align-items-start col-10 offset-md-1">
				<Col md="4" className="">
					<BuildingForm
						token={props.token}
						locations={locations}
						buildings={buildings}

						getLocations={getLocations}
						getBuildings={getBuildings}
					/>
				</Col>
				<Col md="8" className="">
					<div className="d-flex justify-content-end">	
						<div >
							<Button className="btn-sm border mr-1" onClick={ ()=> getBuildings() }>Get All</Button>
							<Button className="btn-sm border mr-1" onClick={ ()=> getBuildings("?isArchived=false") }>Get Active</Button>
							<Button className="btn-sm border mr-1" onClick={ ()=> getBuildings("?isArchived=true") }>Get Archived</Button>
						</div>
					</div>
					<hr/>
					<BuildingTable
						toggle={toggle}
						buildings={buildings}

						updateBuilding={updateBuilding}

						archiveBuilding={archiveBuilding}
						unarchiveBuilding={unarchiveBuilding}

						deleteBuilding={deleteBuilding}
					/>
				</Col>
			</div>
			<BuildingModal
				modal={modal}
				toggle={toggle}
				building={building}
				updateBuilding={updateBuilding}

				setLoading={props.setLoading}
			/>
		</Fragment>
	)
}

export default BuildingPage;