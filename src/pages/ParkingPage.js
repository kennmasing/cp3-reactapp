import React, { Fragment, useState, useEffect } from 'react';
import { Col, Button } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';

import ParkingForm from '../forms/ParkingForm';
import ParkingTable from '../tables/ParkingTable';
import ParkingModal from '../modals/ParkingModal';

const ParkingPage = (props) => {
	
	//----------------USER HOME PAGE------------------

	//------------------LOCATIONS---------------------
	// console.log("LOCATION PROPS TOKEN", props.token)
	const [locationsData, setLocationsData] = useState({
		token: props.token,
		locations: []
	})
	const { token, locations } = locationsData;
	const config = {
		headers: {
			'Authorization' : `Bearer ${token}`
		}
	}
  	// const url = "http://localhost:8000"
  	const getLocations = async () => {
  		try {
  			const res = await axios.get(`${URL}/locations`, config)
  			// console.log("LOCATIONS RESPONSE", res)
  			return setLocationsData({
  				...locationsData,
  				locations: res.data
  			})
  			// console.log("LOCATIONS DATA", locations)
  		} catch(e) {
  			console.log(e.response)
  		}
  	}
  	useEffect(() => {
  		getLocations()
  	}, [])

  	//------------------BUILDINGS---------------------
	const [buildingsData, setBuildingsData] = useState({
		token: props.token,
		buildings: []
	})
	const { buildings } = buildingsData;
  	const getBuildings = async (locId) => {
  		// console.log("BUILDING ID", id)
  		try {
  			const res = await axios.get(`${URL}/buildings${locId}`, config)
  			// console.log("BUILDINGS RESPONSE", res)
  			return setBuildingsData({
  				...buildingsData,
  				buildings: res.data
  			})
  			// console.log("BUILDINGS DATA", buildings)
  		} catch(e) {
  			console.log(e.response)
  		}
  	}

  	//------------------PARKINGS---------------------
	const [parkingsData, setParkingsData] = useState({
		token: props.token,
		parkings: []
	})
	const { parkings } = parkingsData;
  	const getParkings = async (bldgId, query = "") => {
  		// console.log("PARKING ID", id)
  		try {
  			const res = await axios.get(`${URL}/parkings${bldgId}${query}`, config)
  			// console.log("PARKINGS RESPONSE", res)
  			return setParkingsData({
  				...parkingsData,
  				parkings: res.data
  			})
  			// console.log("PARKING DATA", parkings)
  		} catch(e) {
  			console.log(e.response)
  		}
  	}


	//UPDATE Parking: MODAL
	const [modalData, setModalData] = useState({
		modal: false,
		parking: {}
	})
	const { modal, parking } = modalData;
	const toggle = async (id) => {
		console.log(id);
		// alert(id);
		try {
		  if(typeof id === "string") {
		    const res = await axios.get(`${URL}/parkings/${id}/update`, config)
		    console.log("PARKING RESPONSE MODAL", res)
		    return setModalData({
		      modal: !modal,
		      parking: res.data
		    })
		  console.log("PARKING DATA AFTER DATA TOGGLE", parking)
		  }
		  setModalData({
		    ...modalData,
		    modal: !modal
		  })
		} catch(e) {
		  console.log(e)
		}
	}
	useEffect(() => {

	}, [toggle])

	const updateParking = async (id, updates) => {
		console.log("UPDATE PARKING FROM MODAL", id, updates)
		try {
			config.headers["Content-Type"] = "application/json";
			// config = {
			// 	headers: {
			// 		'Content-Type' : 'application/json'
			// 	}
			// }
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/parkings/${id}/update`, body, config)
			return setModalData({
				...modalData,
				modal: !modal
			})
		getParkings()
		//SWAL
		} catch(e) {
			console.log(e)
		}
	}

	// const [parkingData, setParkingData] = useState({
	//   parking: {}
	// })
	// const { parking } = parkingData;
	// const getParking = async () => {
	//   try {
	//     const res = await axios.get(`${URL}/parkings/${props.match.params.id}`, config)
	//     setParkingData({
	//       ...parkingData,
	//       parking: res.data
	//     })
	//   } catch(e) {
	//     console.log(e.response)
	//   }
	// }
	// useEffect(() => {
	//   getParking()
	// }, [setParkingData])

	// console.log("PARKING DATA 2", parking)
   	const enableParking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/parkings/${id}/enable`, body, config)
			getParkings()
		} catch(e) {
			console.log(e)
		}
	}
   	const disableParking = async (id, updates) => {
   		try {
   			config.headers["Content-Type"] = "application/json";
   			const body = JSON.stringify(updates);
   			const res = await axios.patch(`${URL}/parkings/${id}/disable`, body, config)
   			getParkings()
   		} catch(e) {
   			console.log(e)
   		}
   	}
   	const archiveParking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/parkings/${id}/archive`, body, config)
			getParkings()
		} catch(e) {
			console.log(e)
		}
	}
	const unarchiveParking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/parkings/${id}/unarchive`, body, config)
			getParkings()
		} catch(e) {
			console.log(e)
		}
	}
	const deleteParking = async (id) => {
	    try {
	      	const res = await axios.delete(`${URL}/parkings/${id}/delete`, config)
	      	// props.setLoading(false)
	      	getParkings()
	    } catch(e) {
	      	console.log(e)
	      	// props.setLoading(false)
	    }
	}




	return (
		<Fragment>
			<h1 className="container-fluid text-center page-hdr">PARKING PAGE</h1>
			<div className="d-flex justify-content-center align-items-start col-10 offset-md-1">
				<Col md="4" className="">
					<ParkingForm 
						token={props.token}
						locations={locations}
						buildings={buildings}
						parkings={parkings}

						getLocations={getLocations}
						getBuildings={getBuildings}
						getParkings={getParkings}
					/>
				</Col>
				<Col md="8" className="">
					<div className="d-flex justify-content-end">	
						<div >
							<Button className="btn-sm border mr-1" onClick={ ()=> getParkings() }>Get All</Button>
							<Button className="btn-sm border mr-1" onClick={ ()=> getParkings("?isAvailable=true")} >Get Available</Button>
							<Button className="btn-sm border mr-1" onClick={ ()=> getParkings("?isAvailable=false")} >Get Booked</Button>
							<Button className="btn-sm border mr-1" onClick={ ()=> getParkings("?isArchived=false") }>Get Active</Button>
							<Button className="btn-sm border mr-1" onClick={ ()=> getParkings("?isArchived=true") }>Get Archived</Button>
						</div>
					</div>
					<hr/>
					<ParkingTable
						toggle={toggle}
						parkings={parkings}
						
						enableParking={enableParking}
						disableParking={disableParking}

						archiveParking={archiveParking}
						unarchiveParking={unarchiveParking}

						deleteParking={deleteParking}
					/>
					<ParkingModal
						modal={modal}
						toggle={toggle}
						parking={parking}
						updateParking={updateParking}
					/>
				</Col>
			</div>
		</Fragment>
	)
}

export default ParkingPage;