import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col, Button, Form, FormGroup,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import StarRating from '../components/StarRating';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';

import ParkingSelectionCard from '../cards/ParkingSelectionCard';
import ParkingSelectionModal from '../modals/ParkingSelectionModal'

const ParkingSelectionPage = (props) => {
	console.log("PARKING SELECT PAGE TOKEN", props.token)
	// const [parkingsData, setParkingsData] = useState({
	// 	token: props.token,
	// 	parkings: []
	// })
	// const { parkings } = parkingsData;
	// const config = {
	// 	headers: {
	// 		'Authorization' : `Bearer ${token}`
	// 	}
	// }
 //  	// const url = "http://localhost:8000"
 //  	const getParkings = async (id) => {
 //  		// console.log("PARKING ID", id)
 //  		try {
 //  			const res = await axios.get(`${URL}/parkings/${id}`, config)
 //  			// console.log("PARKINGS RESPONSE", res)
 //  			return setParkingsData({
 //  				...parkingsData,
 //  				parkings: res.data
 //  			})
 //  			// console.log("PARKING DATA", parkings)
 //  		} catch(e) {
 //  			console.log(e.response)
 //  		}
 //  	}
 //  	useEffect(() => {
 //  		getParkings()
 //  	}, [setParkingsData])

	// //RATE MODAL
	// //USESTATE
	// const [modalData,  setModalData] = useState({
	// 	modal: false,
	// 	parking: {}
	// })

	// const { modal, parking } = modalData;

	// //TOGGLE FUNCTION
	// const toggle = async (id) => {
	// 	console.log(id);
	// 	alert(id);

	// 	try {	
	// 		if(typeof id === "string") {
	// 			const res = await axios.get(`${URL}/parkings/${id}`)

	// 			return setModalData({
	// 				modal: !modal,
	// 				parking: res.data
	// 			})
	// 			console.log("GET PARKING", res)
	// 		}
	// 		setModalData({
	// 			...modalData,
	// 			modal: !modal
	// 		})
	// 	} catch(e) {
	// 		console.log(e)
	// 	}
	// }

	// //USE EFFECT
	// useEffect(() => {

	// }, [toggle])

	
	return (
		<Fragment>
			<div className="bg-home02 no-gutters">
				<row className="">
					<Col md="4" className="offset-md-4 pt-5">
						{/*<ParkingSelectionCard
							parkings={parkings}
							toggle={toggle}
						/>*/}
						<ParkingSelectionCard token={props.token}/>
					</Col>
				</row>
			</div>
			{/*<ParkingSelectionModal
				modal={modal}
				toggle={toggle}
				parking={parking} 
			/>*/}
		</Fragment>
	)
}

export default ParkingSelectionPage;