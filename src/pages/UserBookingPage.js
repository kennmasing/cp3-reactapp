import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col, Button, Form, FormGroup,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';
import Paginate from '../partials/Paginate';


import UserBookingTable from '../tables/UserBookingTable';
import UserBookingTableMobile from '../tables/UserBookingTableMobile';
import BookingModal  from '../modals/BookingModal';
import BookingModalUser from '../modals/BookingModalUser';

const UserBookingPage = (props) => {
	console.log("PROPS ROLE ID", props.roleId)
	const [bookingsData, setBookingsData] = useState({
		token: props.token,
		bookings: [],
		total: undefined,
		limit: 5
	})
	const { token, bookings, total, limit } = bookingsData;
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}
  	const url = "http://localhost:8000"
	
	const getBookings = async (query = "") => {
		console.log("GET BOOKINGS", bookings)
		try {
			const res = await axios.get(`${URL}/bookings/${props._id}/me?limit=${limit}${query}`, config)
			console.log("BOOKINGS RESPONSE", res)
			return setBookingsData({
				...bookingsData,
				bookings: res.data.bookings,
				total: res.data.total
			})
		} catch(e) {
			console.log(e.response)
	      	//SWAL
		      Swal.fire({
		        title: "Error",
		        text: e.response,
		        icon: "error",
		        showConfirmationButton: false,
		        timer: 3000
		      })
		}
	}
	console.log("BOOKINGS DATA ++++++++", bookings)
	useEffect(() => {
		getBookings()
	}, [setBookingsData])




	//USE STATE
	const [statusesData, setStatusesData] = useState([])
	const getStatuses =  async () => {
	  try {
	    const res = await axios.get(`${URL}/statuses`, config)
	    // console.log("STATUSES RESPONSE", res)
	    return setStatusesData(res.data)
	    // console.log("STATUSES DATA", statusesData)
	  } catch(e) {
	    console.log(e)
	  }
	}
	//USE EFFECT
	useEffect(() => {
	  getStatuses()
	}, [setStatusesData])


	// const [parkingData, setParkingData] = useState({
	//   parking: {}
	// })
	// const { parking } = parkingData;
	// const getParking = async () => {
	//   try {
	//     const res = await axios.get(`${URL}/parkings/${props.match.params.id}`, config)
	//     setParkingData({
	//       ...parkingData,
	//       parking: res.data
	//     })
	//   } catch(e) {
	//     console.log(e.response)
	//   }
	// }
	// useEffect(() => {
	//   getParking()
	// }, [setParkingData])

	
	// console.log("PARKING DATA 2", parking)
   	const enableParking = async (id, updates) => {
   		try {
   			config.headers["Content-Type"] = "application/json";
   			const body = JSON.stringify(updates);
   			const res = await axios.patch(`${URL}/parkings/${id}/enable`)
   		} catch(e) {
   			console.log(e)
   		}
   	}
   	// const disableParking = async (id, updates) => {
   	// 	try {
   			
   	// 	} catch(e) {
   	// 		console.log(e)
   	// 	}
   	// }

	const getBooking = async (query = "") => {
		// console.log("GET BOOKINGS", bookings)
		try {
			const res = await axios.get(`${URL}/bookings/${props.match.params.id}`, config)
			console.log("BOOKINGS RESPONSE", res)
			return setBookingsData({
				...bookingsData,
				bookings: res.data
			})
		} catch(e) {
			console.log(e.response)
	      	//SWAL
		      Swal.fire({
		        title: "Error",
		        text: e.response,
		        icon: "error",
		        showConfirmationButton: false,
		        timer: 3000
		      })
		}
	}
	// console.log("BOOKINGS DATA ++++++++", booking)

	// useEffect(() => {
	// 	getBookings()
	// }, [setBookingsData])

	//MODAL
	const [modalData,  setModalData] = useState({
		modal: false,
		booking: {}
	})
	const { modal, booking } = modalData;
	//TOGGLE FUNCTION
	const toggle = async (id) => {
		// console.log("TOGGLE ID", id);
		// alert(id);
		try {
		  if(typeof id === "string") {
		    const res = await axios.get(`${URL}/bookings/${id}`, config)
		    // console.log("BOOKING DATA OF TOGGLED", res)
		    return setModalData({
		      modal: !modal,
		      booking: res.data
		    })
		    // console.log("TOGGLED BOOKING DATA", booking)
		  }
		  setModalData({
		    ...modalData,
		    modal: !modal
		  })
		} catch(e) {
		  console.log(e)
		}
	}
	useEffect(() => {

	}, [toggle])
	//UPDATE RATE
	const updateBooking = async (id, updates) => {
		// console.log("UPDATE BOOKING", id, updates)
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}`, body, config)
			return setModalData({
				...modalData,
				modal: !modal
			})
		// getBooking()
		//SWAL
		} catch(e) {
			console.log(e)
		}
	}	
	const pendingBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/pending`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const approveBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/approve`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const declineBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/decline`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const payBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/paid`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const unpayBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/unpaid`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const completeBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/completed`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const ongoingBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/ongoing`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const archiveBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/archive`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const unarchiveBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/unarchive`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const deleteBooking = async (id) => {
	    try {
	      	const res = await axios.delete(`${URL}/bookings/${id}/cancel`, config)
	      	// props.setLoading(false)
	      	getBookings()
	    } catch(e) {
	      	// props.setLoading(false)
	      	console.log(e)
	    }
	}

	const displayPagination = () => {
		return <Paginate total={total} limit={limit} getDocuments={getBookings} />
	}

	return (
		<Fragment>
			<h1 className="container-fluid text-center page-hdr">BOOKING HISTORY</h1>
			<div className="landing-desktop">
				<Col md="12" className="no-gutters">
					<UserBookingTable
						toggle={toggle}
						bookings={bookings}

						statuses={statusesData} 
						roleId={props.roleId}

						pendingBooking={pendingBooking}
						approveBooking={approveBooking}
						declineBooking={declineBooking}

						payBooking={payBooking}
						unpayBooking={unpayBooking}

						completeBooking={completeBooking}
						ongoingBooking={ongoingBooking}

						archiveBooking={archiveBooking}
						unarchiveBooking={unarchiveBooking}

						deleteBooking={deleteBooking}
						enableParking={enableParking}

						setLoading={props.setLoading}
					/>
				</Col>
			</div>
			<div className="landing-mobile">
				<Col className="col-12 offset-md-1 d-flex justify-content-center align-items-center ">
					<UserBookingTableMobile 
						toggle={toggle}
						bookings={bookings}

						statuses={statusesData} 
						roleId={props.roleId}

						pendingBooking={pendingBooking}
						approveBooking={approveBooking}
						declineBooking={declineBooking}

						payBooking={payBooking}
						unpayBooking={unpayBooking}

						completeBooking={completeBooking}
						ongoingBooking={ongoingBooking}

						archiveBooking={archiveBooking}
						unarchiveBooking={unarchiveBooking}

						deleteBooking={deleteBooking}
						enableParking={enableParking}

						setLoading={props.setLoading}
					/>
				</Col>
			</div>
			<div className="text-right">
				{ total > limit ? <Paginate getDocuments={getBookings} total={total} limit={limit} /> : "" }
			</div>
			{/*<BookingModal
				modal={modal}
				toggle={toggle}
				booking={booking}
				updateBooking={updateBooking}

				parking={parking}
				enableParking={enableParking}
				completeBooking={completeBooking}
			/>*/}
			<BookingModalUser 
				modal={modal}
				toggle={toggle}
				token={props.token}
				booking={booking}

				getBooking={getBooking}

				statuses={statusesData} 
				roleId={props.roleId}

				pendingBooking={pendingBooking}
				approveBooking={approveBooking}
				declineBooking={declineBooking}

				payBooking={payBooking}
				unpayBooking={unpayBooking}

				completeBooking={completeBooking}
				ongoingBooking={ongoingBooking}

				archiveBooking={archiveBooking}
				unarchiveBooking={unarchiveBooking}

				deleteBooking={deleteBooking}
				enableParking={enableParking}

				setLoading={props.setLoading}	
			/>
		</Fragment>
	)
}

export default UserBookingPage;