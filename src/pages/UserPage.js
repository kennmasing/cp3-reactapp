import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col, Button, Form, FormGroup,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
// import StarRating from '../components/StarRating';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';

// import UserProfileForm from '../forms/UserProfileForm';
import UserTable from '../tables/UserTable';
import UserModal  from '../modals/UserModal';

const UserPage = (props) => {
	console.log("PROPS TOKEN", props.token)
	const [usersData, setUsersData] = useState({
		token: props.token,
		users: []
	})
	const { token, users } = usersData;
	const config = {
		headers: {
			Authorization: `Bearer ${token}`
		}
	}
  	const url = "http://localhost:8000"
	const getUsers = async () => {
		try {
			const res = await axios.get(`${URL}/users`, config)
			console.log("Users RESPONSE", res)
			return setUsersData({
				...usersData,
				users: res.data})
			console.log("Users DATA", users)
		} catch(e) {
			console.log(e.response)
	      	//SWAL
		      Swal.fire({
		        title: "Error",
		        text: e.response.data.message,
		        icon: "error",
		        showConfirmationButton: false,
		        timer: 3000
		      })
		}
	}
	useEffect(() => {
		getUsers()
	}, [setUsersData])
	//MODAL
	const [modalData,  setModalData] = useState({
		modal: false,
		user: {}
	})
	const { modal, user } = modalData;
	//TOGGLE FUNCTION
	const toggle = async (id) => {
		console.log(id);
		alert(id);
		try {	
			if(typeof id === "string") {
				const res = await axios.get(`${URL}/users/${id}`)

				return setModalData({
					modal: !modal,
					user: res.data
				})
				console.log("GET User", res)
			}
			setModalData({
				...modalData,
				modal: !modal
			})
		} catch(e) {
			console.log(e)
		}
	}
	//USE EFFECT
	useEffect(() => {

	}, [toggle])
	//UPDATE RATE
	const updateUser = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/users/${id}`, body, config)
			setModalData({
				...modalData,
				modal: !modal
			})
			getUsers()
		} catch(e) {
			console.log(e)
		}
	}

	const archiveUser = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/users/${id}/archive`, body, config)
			
			getUsers()
		} catch(e) {
			console.log(e)
		}
	}
	const unarchiveUser = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/users/${id}/unarchive`, body, config)
			
			getUsers()
		} catch(e) {
			console.log(e)
		}
	}
	const deleteUser = async (id) => {
	    try {
	      	const res = await axios.delete(`${URL}/users/${id}/delete`, config)
	      	// props.setLoading(false)
	      	getUsers()
	    } catch(e) {
	      	console.log(e)
	      	// props.setLoading(false)
	    }
	}
	
	return (
		<Fragment>
			<h1 className="container-fluid text-center page-hdr">USERS</h1>
			<div className="d-flex justify-content-center align-items-start login-vh col-10 offset-md-1">
				{/*<Col md="4" className="ml-5">
					<h1>User</h1>
				</Col>*/}
				<Col md="12" className="">
					<UserTable
						toggle={toggle}
						users={users}

						archiveUser={archiveUser}
						unarchiveUser={unarchiveUser}

						deleteUser={deleteUser}
					/>
					<UserModal
						modal={modal}
						toggle={toggle}
						user={user}
						updateUser={updateUser}
					/>
				</Col>
			</div>
		</Fragment>
	)
}

export default UserPage;