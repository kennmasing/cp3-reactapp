import React, {Fragment} from 'react';
import { Row, Col } from 'reactstrap';
// import { Link, Redirect } from 'react-router-dom';

import LocationPage from './LocationPage';
import BuildingPage from './BuildingPage';
import ParkingPage from './ParkingPage';

const ResourcePage = (props) => {
	return (
		<Fragment>
			<h1 className="container-fluid text-center">RESOURCES PAGE</h1>
			<Row>
				<LocationPage />
			</Row>
			<Row>
				<BuildingPage />
			</Row>
			<Row>
				<ParkingPage />
			</Row>
		</Fragment>
	)
}

export default ResourcePage;