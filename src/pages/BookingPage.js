import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col, Button, Form, FormGroup,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';
import Paginate from '../partials/Paginate';

import BookingCard from  '../cards/BookingCard';

import BookingTable from '../tables/BookingTable';
import BookingTableMobile from '../tables/BookingTableMobile';
import BookingModal  from '../modals/BookingModal';
import BookingModalSuperAdmin from '../modals/BookingModalSuperAdmin';

const BookingPage = (props) => {
	// console.log("PROPS TOKEN", props.token)
	const [bookingsData, setBookingsData] = useState({
		token: props.token,
		bookings: [],
		total: undefined,
		limit: 5
	})
	const { token, bookings, total, limit } = bookingsData;
	const config = {
		headers: {
			Authorization: `Bearer ${props.token}`
		}
	}
	
	const getBookings = async (query = "") => {
		console.log("GET BOOKINGS", bookings)
		try {

			// if(localStorage.roleId == 1 || localStorage.roleId == 2) {
				const res = await axios.get(`${URL}/bookings/history?limit=${limit}${query}`, config)
				// console.log("bookings RESPONSE", res)
				return setBookingsData({
					...bookingsData,
					bookings: res.data.bookings,
					total: res.data.total
				})
			// console.log("bookings DATA", bookings)
			// } else if(localStorage.roleId == 3) {
			// 	const res = await axios.get(`${URL}/bookings/${localStorage._id}/me`, config)
			// 	// console.log("bookings RESPONSE", res)
			// 	return setBookingsData({
			// 		...bookingsData,
			// 		bookings: res.data
			// 	})
			// }
		} catch(e) {
			console.log(e.response)
	      	//SWAL
		      Swal.fire({
		        title: "Error",
		        text: e.response,
		        icon: "error",
		        showConfirmationButton: false,
		        timer: 3000
		      })
		}
	}
	console.log("BOOKINGS DATA", bookings)
	useEffect(() => {
		getBookings()
	}, [])


	//USE STATE
	const [statusesData, setStatusesData] = useState([])
	const getStatuses =  async () => {
	  try {
	    const res = await axios.get(`${URL}/statuses`, config)
	    // console.log("STATUSES RESPONSE", res)
	    return setStatusesData(res.data)
	    // console.log("STATUSES DATA", statusesData)
	  } catch(e) {
	    console.log(e)
	  }
	}
	//USE EFFECT
	useEffect(() => {
	  getStatuses()
	}, [setStatusesData])


	const [parkingData, setParkingData] = useState({
	  parking: {}
	})
	const { parking } = parkingData;
	const getParking = async () => {
	  try {
	    const res = await axios.get(`${URL}/parkings/${props.match.params.id}`, config)
	    setParkingData({
	      ...parkingData,
	      parking: res.data
	    })
	  } catch(e) {
	    console.log(e.response)
	  }
	}
	useEffect(() => {
	  getParking()
	}, [setParkingData])

	
	// console.log("PARKING DATA 2", parking)
   	const enableParking = async (id, updates) => {
   		try {
   			config.headers["Content-Type"] = "application/json";
   			const body = JSON.stringify(updates);
   			const res = await axios.patch(`${URL}/parkings/${id}/enable`)
   		} catch(e) {
   			console.log(e)
   		}
   	}
   	// const disableParking = async (id, updates) => {
   	// 	try {
   			
   	// 	} catch(e) {
   	// 		console.log(e)
   	// 	}
   	// }

	//MODAL
	const [modalData,  setModalData] = useState({
		modal: false,
		booking: {}
	})
	const { modal, booking } = modalData;
	//TOGGLE FUNCTION
	const toggle = async (id) => {
		console.log("TOGGLE ID", id);
		// alert(id);
		try {
		  if(typeof id === "string") {
		    const res = await axios.get(`${URL}/bookings/${id}`, config)
		    // console.log("BOOKING DATA OF TOGGLED", res)
		    return setModalData({
		      modal: !modal,
		      booking: res.data
		    })
	      	console.log("TOGGLED BOOKING DATA", booking)
		  }
		  setModalData({
		    ...modalData,
		    modal: !modal
		  })
		} catch(e) {
		  console.log(e)
		}
	}
	useEffect(() => {

	}, [toggle])
	//UPDATE RATE
	const updateBooking = async (id, updates) => {
		// console.log("UPDATE BOOKING", id, updates)
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}`, body, config)
			return setModalData({
				...modalData,
				modal: !modal
			})
		// getBooking()
		//SWAL
		} catch(e) {
			console.log(e)
		}
	}	
	const pendingBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/pending`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const approveBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/approve`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const declineBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/decline`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const payBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/paid`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const unpayBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/unpaid`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const completeBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/completed`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const ongoingBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/ongoing`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const archiveBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/archive`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const unarchiveBooking = async (id, updates) => {
		try {
			config.headers["Content-Type"] = "application/json";
			const body = JSON.stringify(updates);
			const res = await axios.patch(`${URL}/bookings/${id}/unarchive`, body, config)
			getBookings()
		} catch(e) {
			console.log(e)
		}
	}
	const deleteBooking = async (id) => {
	    try {
	      	const res = await axios.delete(`${URL}/bookings/${id}/cancel`, config)
	      	// props.setLoading(false)
	      	getBookings()
	    } catch(e) {
	      	// props.setLoading(false)
	      	console.log(e)
	    }
	}

	const displayPagination = () => {
		return <Paginate total={total} limit={limit} getDocuments={getBookings} />
	}

	let filter = ""
	if(localStorage.roleId == 1 || localStorage.roleId == 2) {
		filter = (
			<Fragment>
			<div className="d-flex justify-content-end">	
				<div >
					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings() }>Get All</Button>
					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?isPaid=true")} >Get Paid</Button>
					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?isPaid=false")} >Get Unpaid</Button>

					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?isCompleted=true")} >Get Completed</Button>
					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?isCompleted=false")} >Get On-going</Button>

					{/*<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?statusId{statusCode:1}")} >Get Pending</Button>
					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?statusId=2")} >Get Approved</Button>
					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?statusId=3")} >Get Declined</Button>*/}

					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?isArchived=false") }>Get Active</Button>
					<Button className="btn-sm border mr-1" onClick={ ()=> getBookings("?isArchived=true") }>Get Archived</Button>

				</div>
			</div>
			<hr/>
			</Fragment>
		)
	} else if(localStorage.roleId == 3) {
		// filter = (

		// )
	}


	return (
		<Fragment>
			<h1 className="container-fluid text-center page-hdr">BOOKING HISTORY</h1>
			<div className="landing-desktop">
				<Col md="12" className="no-gutters">
					{filter}
					<BookingTable
						toggle={toggle}
						bookings={bookings}

						statuses={statusesData} 
						roleId={props.roleId}

						pendingBooking={pendingBooking}
						approveBooking={approveBooking}
						declineBooking={declineBooking}

						payBooking={payBooking}
						unpayBooking={unpayBooking}

						completeBooking={completeBooking}
						ongoingBooking={ongoingBooking}

						archiveBooking={archiveBooking}
						unarchiveBooking={unarchiveBooking}

						deleteBooking={deleteBooking}
						enableParking={enableParking}

						setLoading={props.setLoading}
					/>
				</Col>
			</div>
			<div className="landing-mobile">
				<Col className="col-12 offset-md-1 d-flex justify-content-center align-items-center ">
					<BookingTableMobile 
						toggle={toggle}
						bookings={bookings}

						statuses={statusesData} 
						roleId={props.roleId}

						pendingBooking={pendingBooking}
						approveBooking={approveBooking}
						declineBooking={declineBooking}

						payBooking={payBooking}
						unpayBooking={unpayBooking}

						completeBooking={completeBooking}
						ongoingBooking={ongoingBooking}

						archiveBooking={archiveBooking}
						unarchiveBooking={unarchiveBooking}

						deleteBooking={deleteBooking}
						enableParking={enableParking}

						setLoading={props.setLoading}
					/>
				</Col>
			</div>
			<div className="text-right">
				{ total > limit ? <Paginate getDocuments={getBookings} total={total} limit={limit} /> : "" }
			</div>

			{/*<div className="landing-mobile">
				<Col className="col-12 no-gutters row d-flex flex-wrap justify-content-center align-items-center">
					<BookingCard 
						toggle={toggle}
						bookings={bookings}

						statuses={statusesData} 
						roleId={props.roleId}

						pendingBooking={pendingBooking}
						approveBooking={approveBooking}
						declineBooking={declineBooking}

						payBooking={payBooking}
						unpayBooking={unpayBooking}

						completeBooking={completeBooking}
						ongoingBooking={ongoingBooking}

						archiveBooking={archiveBooking}
						unarchiveBooking={unarchiveBooking}

						deleteBooking={deleteBooking}
						enableParking={enableParking}

						setLoading={props.setLoading}
					/>
				</Col>
			</div>*/}

			{/*<BookingModal
				modal={modal}
				toggle={toggle}
				booking={booking}
				updateBooking={updateBooking}

				parking={parking}
				enableParking={enableParking}
				completeBooking={completeBooking}
			/>*/}

			<BookingModalSuperAdmin 
				modal={modal}
				toggle={toggle}
				token={props.token}
				booking={booking}

				statuses={statusesData} 
				roleId={props.roleId}

				pendingBooking={pendingBooking}
				approveBooking={approveBooking}
				declineBooking={declineBooking}

				payBooking={payBooking}
				unpayBooking={unpayBooking}

				completeBooking={completeBooking}
				ongoingBooking={ongoingBooking}

				archiveBooking={archiveBooking}
				unarchiveBooking={unarchiveBooking}

				deleteBooking={deleteBooking}
				enableParking={enableParking}

				setLoading={props.setLoading}	
			/>
		</Fragment>
	)
}

export default BookingPage;