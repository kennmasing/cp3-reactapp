import React, { Fragment } from 'react';
// import { Table, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import Swal from 'sweetalert2';

const LocationRow = (props) => {

const {
  toggle,
  location,
  index,
  archiveLocation,
  unarchiveLocation,
  deleteLocation
} = props


const { _id, name, isArchived, createdAt } = location

const onClickArchiveHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isArchived
  }

  if(isArchived == false) {
    archiveLocation(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Location Archived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  } else if(isArchived == true) {
    unarchiveLocation(_id, updates)
     Swal.fire({
      title: "Success",
      text: "Location Unarchived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
}

const onClickDeleteHandler = async (e) => {
  e.preventDefault()
  deleteLocation(_id)
}


let archival = ""
if(location.isArchived == false) {
   archival = (
    <td><Button className="mb-1 btn btn-sm btn-success btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } > ACTIVE </Button></td>
  )
} else {
  archival =(
   <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } >  ARCHIVED </Button></td>
  )
}


  return (
    <Fragment>
        <tr>
          <th className="text-center">{index}</th>
          <td className="text-left">{name.toUpperCase()}</td>
          <td className="text-center">{moment(createdAt).fromNow()}</td>
          {/*<td className="text-center">{isArchived.toString()}</td>*/}
          { archival }
          <td className="text-center">
              <Button className="mb-1 btn btn-sm btn-warning ml-1" onClick={ e => onClickDeleteHandler(e) } ><i class="fas fa-trash-alt"></i></Button> {/*CANCEL*/}
              {/*<Button className="mb-1 btn btn-sm btn-success ml-1" onClick={() => toggle(_id) } ><i class="fas fa-trash-alt"></i></Button> {/*CANCEL*/}
              {/*<Button className="mb-1 btn btn-success mx-2" onClick={()=> toggle(_id)}>Update</Button>*/}
          </td>
        </tr>
    </Fragment>
  );
}

export default LocationRow;