import React, { Fragment, useState, useEffect } from 'react';
// import { Table, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import Swal from 'sweetalert2';

const BuildingRow = (props) => {

const {
  toggle,
  building,
  index,
  archiveBuilding,
  unarchiveBuilding,
  deleteBuilding
} = props

const { _id, name, locationId, isArchived, createdAt } = building
const [ isRedirected, setIsRedirected ] = useState(false);

const onClickArchiveHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isArchived
  }

  if(isArchived == false) {
    archiveBuilding(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Building Archived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  } else if(isArchived == true) {
    unarchiveBuilding(_id, updates)
     Swal.fire({
      title: "Success",
      text: "Building Unarchived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
}

const onClickDeleteHandler = async (e) => {
  e.preventDefault()
    Swal.fire({
      title: "Success",
      text: "Building Deleted",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  deleteBuilding(_id)
}



let archival = ""
if(building.isArchived == false) {
   archival = (
    <td><Button className="mb-1 btn btn-sm btn-success btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } > ACTIVE </Button></td>
  )
} else {
  archival =(
   <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } >  ARCHIVED </Button></td>
  )
}

  return (
    <Fragment>
        <tr>
          <th className="text-center">{index}</th>
          <td className="text-left">{name.toUpperCase()}</td>
          <td className="text-center">{moment(createdAt).fromNow()}</td>
          <td className="text-center">{locationId.name.toUpperCase()}</td>
          {archival}
          <td className="text-center">
              <Button className="mb-1 btn btn-sm btn-warning ml-1" onClick={ e => onClickDeleteHandler(e) } ><i class="fas fa-trash-alt"></i></Button> {/*CANCEL*/}
              {/*<Button className="mb-1 btn btn-sm btn-success ml-1" onClick={() => toggle(_id) } ><i class="fas fa-trash-alt"></i></Button> {/*CANCEL*/}
              {/*<Link to="" className="mb-1 btn btn-info mx-2"><i className="fas fa-eye"></i></Link>*/}
          </td>
        </tr>
    </Fragment>
  );
}

export default BuildingRow;