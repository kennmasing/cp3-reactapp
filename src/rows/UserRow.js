import React, { Fragment, useState, useEffect } from 'react';
// import { Table, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import Swal from 'sweetalert2';

const UserRow = (props) => {
  const {
    toggle,
    user,
    index,
    archiveUser,
    unarchiveUser,
    deleteUser
  } = props


  const { _id, name, firstName, lastName, username, email, roleId, isArchived } = user
  const [ isRedirected, setIsRedirected ] = useState(false);

  const onClickArchiveHandler = async (e) => {
    e.preventDefault();
    const updates = {
      isArchived
    }

    if(isArchived == false) {
      archiveUser(_id, updates)
      Swal.fire({
        title: "Success",
        text: "User Archived",
        icon: "success",
        showConfirmationButton: false,
        timer: 3000
      })
    } else if(isArchived == true) {
      unarchiveUser(_id, updates)
       Swal.fire({
        title: "Success",
        text: "User Unarchived",
        icon: "success",
        showConfirmationButton: false,
        timer: 3000
      })
    }
  }

  const onClickDeleteHandler = async (e) => {
  e.preventDefault();
  // setLoading(true)
  Swal.fire({
      title: "Success",
      text: "User Deleted",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  deleteUser(_id)

}

  let archival = ""
  if(user.isArchived == false) {
     archival = (
      <td  className="text-center"><Button className="mb-1 btn btn-sm btn-success btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } > ACTIVE </Button></td>
    )
  } else {
    archival =(
     <td  className="text-center"><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } >  ARCHIVED </Button></td>
    )
  }

  let role = ""
  if(roleId == 1) {
    role = (
     <td className="text-center">SUPER ADMIN</td>
    )
  } else if(roleId == 2) {
    role =(
    <td className="text-center">ADMIN</td>
    )
  } else if(roleId == 3) {
    role = (
    <td className="text-center">USER</td>
    )
  }

  return (
    <Fragment>
        <tr>
          <th className="text-center">{index}</th>
          <td className="text-center">{lastName.toUpperCase()}</td>
          <td className="text-center">{firstName}</td>
          <td className="text-center">{username}</td>
          <td className="text-center">{email}</td>
            {role}
            {archival}
          <td className="text-center">
              {/*<Link to="" className="mb-1 btn btn-info mx-2"><i className="fas fa-eye"></i></Link>*/}
              <Button className="mb-1 btn btn-sm btn-warning ml-1" onClick={ e => onClickDeleteHandler(e) } ><i class="fas fa-trash-alt"></i></Button> {/*CANCEL*/}
          </td>
        </tr>
    </Fragment>
  );
}

export default UserRow;