import React, { Fragment, useState, useEffect } from 'react';
// import { Table, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import Swal from 'sweetalert2';

const ParkingRow = (props) => {

const {
  toggle,
  parking,
  index,
  enableParking,
  disableParking,
  archiveParking,
  unarchiveParking,
  deleteParking
} = props

const { _id, name, isArchived, createdAt, buildingId, isAvailable } = parking

const [isRedirected, setIsRedirected] = useState(false)

// console.log("BUILDING ID IN PARKING ROW", buildingId)

const onClickEnableHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isAvailable
  }
  if(isAvailable == false) {
    enableParking(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Parking slot enabled.",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
    setIsRedirected(true)
  } else if(isAvailable == true) {
   disableParking(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Parking slot disabled",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
    setIsRedirected(true)
  }
}

const onClickArchiveHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isArchived
  }

  if(isArchived == false) {
    archiveParking(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Parking Archived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
    setIsRedirected(true)
  } else if(isArchived == true) {
    unarchiveParking(_id, updates)
     Swal.fire({
      title: "Success",
      text: "Parking Unarchived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
    setIsRedirected(true)
  }
}

const onClickDeleteHandler = async (e) => {
  e.preventDefault()

    try {
      deleteParking(_id)
      //  Swal.fire({
      //   title: "Success",
      //   text: "Building Deleted",
      //   icon: "success",
      //   showConfirmationButton: false,
      //   timer: 3000
      // })
      setIsRedirected(true)
    } catch(e) {
      console.log(e)
    }
}

// if(isRedirected) {
//   return window.location = "/parkings" 
// }



let availability = ""
if(isAvailable == false) {
  availability = (
   <td><Button className="mb-1 btn btn-sm btn-warning btn-block mx-1" onClick={ e => onClickEnableHandler(e) } > BOOKED </Button></td>
  )
} else if(isAvailable == true) {
  availability = (
   <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickEnableHandler(e) } > AVAILABLE </Button></td>
  )
}

let archival = ""
if(parking.isArchived == false) {
   archival = (
    <td><Button className="mb-1 btn btn-sm btn-success btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } > ACTIVE </Button></td>
  )
} else {
  archival =(
   <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } >  ARCHIVED </Button></td>
  )
}

  return (
    <Fragment>
        <tr>
          <th className="text-center">{index}</th>
          <td className="text-left">SLOT {name.toUpperCase()}</td>
          <td className="text-center">{moment(createdAt).fromNow()}</td>
          <td className="text-center">{parking.buildingId.name.toUpperCase()}</td>
          {availability}
          {archival}
          <td className="text-center">
              <Button className="mb-1 btn btn-sm btn-warning ml-1" onClick={ e => onClickDeleteHandler(e) } ><i class="fas fa-trash-alt"></i></Button> {/*CANCEL*/}
              {/*<Button className="mb-1 btn btn-sm btn-info btn-blocked mx-2" onClick={()=> toggle(_id)}>UPDATE</Button>*/}
          </td>
        </tr>
    </Fragment>
  );
}

export default ParkingRow;