import React, { Fragment, useState, useEffect } from 'react';
// import { Table, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import moment from 'moment';
import axios from 'axios';
import Swal from 'sweetalert2'
import { URL } from '../config'

const BookingRow = (props) => {

const {
  toggle,
  booking,
  index,
  roleId,
  token,
  pendingBooking,
  approveBooking,
  declineBooking,
  payBooking,
  unpayBooking,
  completeBooking,
  ongoingBooking,
  archiveBooking,
  unarchiveBooking,
  deleteBooking,
  enableParking,
  setLoading
} = props

const { _id, name, parkingId, isPaid, statusId, isCompleted, isArchived, createdAt, startDate, endDate } = booking
const [ disabledBtn, setDisabledBtn ] = useState(false)
const [ disabledDeclineBtn, setDisabledDeclineBtn ] = useState(true)

//USE STATE
// const [parkingData, setParkingData] = useState({
//   parking: {}
// })
// const { parking } = parkingData
// const config = {
//   headers: {
//     Authorization: `Bearer ${token}`
//   }
// }
// const getParking = async (id) => {
//   try {
//     const res = await axios.get(`${URL}/parkings/${id}`, config)
//     return setParkingData({
//       ...parkingData,
//       parking: res.data
//     })
//     console.log("PARKING IN BOOKING ROW", parking)
//   } catch(e) {
//     console.log(e)
//   }
// }

// useEffect(() => {
//   getParking()
// }, [setParkingData])

//DESTRUCTURE

  // const url = "http://localhost:8000"

  // const enableParking = async



const onClickPendingHandler = async (e) => {
  e.preventDefault();
  const updates = {
    statusId
  }
  pendingBooking(_id, updates)
  Swal.fire({
    title: "Success",
    text: "Booking Pending",
    icon: "success",
    showConfirmationButton: false,
    timer: 3000
  })
}

const onClickApproveHandler = async (e) => {
  e.preventDefault();
  const updates = {
    statusId
  }
  approveBooking(_id, updates)
  Swal.fire({
    title: "Success",
    text: "Booking Approved",
    icon: "success",
    showConfirmationButton: false,
    timer: 3000
  })
}

const onClickDeclineHandler = async (e) => {
  e.preventDefault();
  const updates = {
    statusId
  }
  declineBooking(_id, updates)
  Swal.fire({
    title: "Success",
    text: "Booking Declined",
    icon: "success",
    showConfirmationButton: false,
    timer: 3000
  })
}

const onClickPaymentHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isPaid
  }
  if(isPaid == false) {
    payBooking(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Paid.",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })

  } else if(isPaid == true) {
    unpayBooking(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Reversed",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
}

const onClickCompletedHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isCompleted
  }

  if(isCompleted == false) {
    completeBooking(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Completed",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  } else if(isCompleted == true) {
    ongoingBooking(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Ongoing",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
  // const parkingUpdates = {
  //   isAvailable
  // }
  // enableParking(parkingId, parkingUpdates)
}

const onClickArchiveHandler = async (e) => {
  e.preventDefault();
  const updates = {
    isArchived
  }

  if(isArchived == false) {
    archiveBooking(_id, updates)
    Swal.fire({
      title: "Success",
      text: "Booking Archived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  } else if(isArchived == true) {
    unarchiveBooking(_id, updates)
     Swal.fire({
      title: "Success",
      text: "Booking Unarchived",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  }
}

const onClickDeleteHandler = async (e) => {
  e.preventDefault();
  // setLoading(true)
  Swal.fire({
      title: "Success",
      text: "Booking Deleted",
      icon: "success",
      showConfirmationButton: false,
      timer: 3000
    })
  deleteBooking(_id)

}

// useEffect(() => {
//   if(roleId == 2 )
// }, [booking])

//USE EFFECT
useEffect(() => {
  if(roleId == 2 && booking.statusId.name == "pending" && booking.isPaid == false) {
    setDisabledBtn(true) 
  } else if(roleId == 2 && booking.statusId.name == "pending" && booking.isPaid == true) {
    setDisabledBtn(false)
  } else {
    setDisabledBtn(true)
  }
}, [booking])

useEffect(() => {
  if(roleId == 2 && booking.statusId.name == "pending" && booking.isPaid == false) {
    setDisabledDeclineBtn(false) 
  } else if(roleId == 2 && booking.statusId.name == "pending" && booking.isPaid == true) {
    setDisabledDeclineBtn(true)
  } else if(roleId == 2 && booking.statusId.name == "approved" && booking.isPaid == true) {
    setDisabledDeclineBtn(true)
  } else {
    setDisabledDeclineBtn(false)
  }
}, [booking])

useEffect(() => {
  if(roleId == 3 && booking.statusId.name == "pending") {
    setDisabledBtn(false) 
  } else if(roleId == 3 && booking.statusId.name == "aprroved") {
    setDisabledBtn(true)
  } else if(roleId == 3 && booking.statusId.name == "declined") {
    setDisabledBtn(true)
  }
}, [booking])

useEffect(() => {
  if(roleId == 2 && booking.statusId.name == "declined" && booking.isPaid == false) {
    setDisabledBtn(true) 
  } else if(roleId == 2 && booking.statusId.name == "declined" && booking.isCompleted == false) {
    setDisabledBtn(true)
  } else {
    setDisabledBtn(false)
  }
}, [booking])


let payment = ""
if(booking.isPaid == false) {
  payment = (
   <td><Button className="mb-1 btn btn-sm btn-warning btn-block mx-1" onClick={ e => onClickPaymentHandler(e) } disabled={disabledBtn}> UNPAID </Button></td>
  )
} else if(booking.isPaid == true) {
  payment = (
   <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickPaymentHandler(e) } disabled> PAID </Button></td>
  )
}

let complete = ""
if(booking.isCompleted == false) {
  complete = (
     <td><Button className="mb-1 btn btn-sm btn-warning btn-block mx-1" onClick={ e => onClickCompletedHandler(e) } disabled={disabledBtn}> ON-GOING </Button></td>
  )
} else {
  complete = (
     <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickCompletedHandler(e) } disabled>  COMPLETED </Button></td>
  )
}

let archival = ""
if(booking.isArchived == false) {
   archival = (
    <td><Button className="mb-1 btn btn-sm btn-success btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } > ACTIVE </Button></td>
  )
} else {
  archival =(
   <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" onClick={ e => onClickArchiveHandler(e) } disabled>  ARCHIVED </Button></td>
  )
}

// let approval = ""
// if(booking.statusId.name == "pending") {
//   approval = (
//     <td><Button className="mb-1 btn btn-sm btn-success btn-block mx-1" disabled>{booking.statusId.name.toUpperCase()}</Button></td>
//   )
// } else if(booking.statusId.name == "approved") {
//   approval = (
//     <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" disabled>{booking.statusId.name.toUpperCase()}</Button></td>
//   )
// } else if(booking.statusId.name == "declined") {
//   approval = (
//     <td><Button className="mb-1 btn btn-sm btn-secondary btn-block mx-1" disabled>{booking.statusId.name.toUpperCase()}</Button></td>
//   )
// } 

let approval = ""
if(booking.statusId.name == "pending") {
  approval = (
    <em className="mb-1 mx-1" disabled>{booking.statusId.name.toUpperCase()}</em>
  )
} else if(booking.statusId.name == "approved") {
  approval = (
    <em className="mb-1 mx-1" disabled>{booking.statusId.name.toUpperCase()}</em>
  )
} else if(booking.statusId.name == "declined") {
  approval = (
    <em className="mb-1 mx-1" disabled>{booking.statusId.name.toUpperCase()}</em>
  )
} 

// let parkingStatus = ""
// if(booking.parkingId.isAvailable.toString().toUpperCase() === false) {
//   parkingStatus =(
//    <h6> SLOT BOOKED </h6>
//   )
// } else if(booking.parkingId.isAvailable.toString().toUpperCase() === true) {
//   parkingStatus =(
//   <h6> SLOT AVAILABLE </h6>
//   )
// }

let F1 = ""
if(booking.endDate == null){
  F1 = (
   <em>Date:</em>
  )
}else if(booking.endDate !== null){
  F1 = (
  <em>From:</em>
  )
}

let T1 = ""
if(booking.endDate == null){
  T1 = (
   <em>Time:</em>
  )
}else if(booking.endDate !== null){
  T1 = (
  <em>Until:</em>
  )
}

let T2 = ""
if(booking.endDate == null){
  T2 = (
   <p>{moment(booking.startDate).format("hh:mm A")}</p>
  )
}else if(booking.endDate !== null){
  T2 = (
  <p>{booking.endDate ? moment(booking.endDate).format('MMMM DD, YYYY') : "N/A"} </p>
  )
}

let tableData = "";
if(roleId == 1) {
   tableData = (
    <Fragment className="d-flex">
      <th className="text-center">{index}</th>
      <td className="text-center">
        {moment(booking.startDate).format('MMMM DD, YYYY')}
        <br/>
        {booking.buildingId.name.toUpperCase()}
        <br/>
        {booking.locationId.name.toUpperCase()}
        
      </td>
      <td className="text-center">
        { approval }
        <br/>
        <Button className="mb-1 btn btn-sm btn-block btn-success ml-1" onClick={() => toggle(_id) } >View More Details</Button>
      </td>
    </Fragment>
  )
} else if(roleId == 2) {
   tableData = (
    <Fragment className="d-flex">
      <th className="text-center">{index}</th>
      <td className="text-center">
        {moment(booking.startDate).format('MMMM DD, YYYY')}
        <br/>
        {booking.buildingId.name.toUpperCase()}
        <br/>
        {booking.locationId.name.toUpperCase()}
      </td>
      <td className="text-center">
        { approval }
        <br/>
        <Button className="mb-1 btn btn-sm btn-success ml-1" onClick={() => toggle(_id) } >View Details</Button>
      </td>
    </Fragment>
  )
}

// console.log("SLOT AVAILABILITY", booking.parkingId.isAvailable)

  return (
    <Fragment>
        <tr>
         { tableData }
        </tr>
    </Fragment>
  );
}

export default BookingRow;