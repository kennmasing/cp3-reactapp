//IMPORT DEPENDENCIES
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./bootstrap.css"; //bootswatch
import "./style.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Swal from 'sweetalert2';
import Loader from 'react-loader-spinner';
import { Container, Row, Col } from 'reactstrap';
import { ParallaxProvider } from 'react-scroll-parallax';

//IMPORT PAGES
import AppNavbar from './partials/AppNavbar';
import LandingPage from './pages/LandingPage';
import RegisterPage from './pages/RegisterPage';
import LoginPage from './pages/LoginPage';
import BookingPage from './pages/BookingPage';
import UserBookingPage from './pages/UserBookingPage';
import CheckoutPage from './pages/CheckoutPage';
import HomePage from './pages/HomePage';
import NotFoundPage from './pages/NotFoundPage';
import ResourcePage from './pages/ResourcePage';
import LocationPage from './pages/LocationPage';
import BuildingPage from './pages/BuildingPage';
import ParkingPage from './pages/ParkingPage';
import ParkingSelectionPage from './pages/ParkingSelectionPage';
import UserPage from './pages/UserPage';
import UserProfilePage from './pages/UserProfilePage';

//CONST APP
const App = () => {
	console.log("APP NAVBAR TOKEN", localStorage.token)
	//DECLARE STATE
	const [appData, setAppData] = useState({
		token: localStorage.token,
		_id: localStorage._id,
		roleId: localStorage.roleId,
		firstName: localStorage.firstName,
		lastName: localStorage.lastName,
		username: localStorage.username,
		email: localStorage.email
	})

	//DESTRUCTURE APP DATA
	const { token, _id, roleId, firstName, lastName, username, email } = appData;


	const [loading, setLoading] = useState(false);

	if(loading) {
		return (
			<Container className="App">
				<Row>
					<Col id="loaderContainer" >
						<div id="loader">
							<Loader
								type="RevolvingDot"
								color="#00BFFF"
								height={200}
								width={200}
							/>
						</div>
					</Col>
				</Row>
			</Container>
		)
	}

	//CREATE A FUNCTION FOR CURRENT USER
	const getCurrentUser = () => {
		return { token, _id, roleId, firstName, lastName, username, email }
	}

	//LOAD parameters(attr to be loaded, destination)
	const Load = (props, page) => {

		//LANDING PAGE
		if(page === "LandingPage" && token) {
			return  <Redirect to="/home" token={token} roleId={roleId}/>
		} else if(page === "LandingPage") {
			return <LandingPage {...props} token={token} roleId={roleId} setLoading={setLoading}/>
		}//REFACTOR FOR LANDING


		//LOGIN PAGE
		if(page === "LoginPage" && token) {
			return <Redirect to="/" />
		} else if(page === "LoginPage") {
			return <LoginPage {...props} setLoading={setLoading}/>
		}

		//REGISTER PAGE
		if(page === "RegisterPage" && token) {
			return <Redirect to="/" />
		} else if(page === "RegisterPage") {
			return <RegisterPage {...props} setLoading={setLoading}/>
		}

		if(!token) return <Redirect to="/login" />

		if( page === "LogoutPage" ) {
			localStorage.clear()
			setAppData({
				token,
				username
			})
			setLoading(true)

			Swal.fire({
	          title: "Success",
	          text: "Logout successful!",
	          icon: "success",
	          showConfirmationButton: false,
	          timer: 3000
	        })

			return window.location = "/"
		}

		switch(page) {
			case "HomePage": return <HomePage {...props} token={token} roleId={roleId} currentUser={getCurrentUser} setLoading={setLoading}/>
			case "BookingPage": return <BookingPage {...props} token={token} roleId={roleId} currentUser={getCurrentUser} setLoading={setLoading} />
			case "UserBookingPage": return <UserBookingPage {...props} _id={_id} token={token} roleId={roleId} currentUser={getCurrentUser} setLoading={setLoading} />
			case "ResourcePage": return <ResourcePage {...props} token={token} setLoading={setLoading}/>
			case "LocationPage": return <LocationPage {...props} token={token} setLoading={setLoading}/>
			case "BuildingPage": return <BuildingPage {...props} token={token} setLoading={setLoading}/>
			case "ParkingPage": return <ParkingPage {...props} token={token} setLoading={setLoading}/>
			case "ParkingSelectionPage": return <ParkingSelectionPage {...props} token={token} setLoading={setLoading}/>
			case "CheckoutPage": return <CheckoutPage {...props} token={token} setLoading={setLoading}/>
			case "UserPage": return <UserPage {...props} token={token} setLoading={setLoading}/>
			case "UserProfilePage": return <UserProfilePage {...props} token={token} setLoading={setLoading}/>
			default:  return <NotFoundPage/>
		}
	}

	let navbar = ""

	if(!token) {

	} else {
		navbar = (
				<AppNavbar token={token} currentUser={firstName} roleId={roleId} className="bg-secondary" _id={_id}/>
		)
	}

	return (
		<ParallaxProvider>
		<BrowserRouter>
			{ navbar }
				<Switch>
					<Route exact path="/" render={ (props)=> Load(props, "LandingPage") } />
					<Route exact path="/home" render={ (props)=> Load(props, "HomePage") } />
					<Route path="/logout" render={ (props)=> Load(props, "LogoutPage") } />
					<Route path="/login" render={ (props)=> Load(props, "LoginPage") } />
					<Route path="/register" render={ (props)=> Load(props, "RegisterPage") } />
					<Route path="/users" render={ (props)=> Load(props, "UserPage") } />
					<Route path="/me" render={ (props)=> Load(props, "UserProfilePage") } />
					<Route path="/bookings" render={ (props)=> Load(props, "BookingPage") } />
					<Route path="/mybookings" render={ (props)=> Load(props, "UserBookingPage") } />
					<Route path="/resources" render={ (props)=> Load(props, "ResourcePage") } />
					<Route path="/locations" render={ (props)=> Load(props, "LocationPage") } />
					<Route path="/buildings" render={ (props)=> Load(props, "BuildingPage") } />
					<Route path="/parkings" render={ (props)=> Load(props, "ParkingPage") } />
					<Route path="/parkingSelection" render={ (props)=> Load(props, "ParkingSelectionPage") } />
					<Route path="/checkout" render={ (props)=> Load(props, "CheckoutPage") } />
					<Route path="*" render={ (props)=> Load(props, "NotFoundPage") } />
				</Switch>
		</BrowserRouter>
		</ParallaxProvider>
	)
}

//EXPORT FUNCTION
export default App;