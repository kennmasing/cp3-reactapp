import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col,
	Button, Form, FormGroup, Label, Input,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';


const LocationForm = (props) => {
	
	const [formData, setFormData] = useState({
		name: ""
	})

	const [disabledBtn, setDisabledBtn] = useState(true); //BUTTON IS CURRENTLY DISABLED
	const [isRedirected, setIsRedirected] = useState(false); //NOT REDIRECTED UNTIL REGISTER & SUBMIT
	const { name } = formData

	const onChangeHandler = e => {
		setFormData({
			...formData,
			[e.target.name] : e.target.value
		})
	}

	const onSubmitHandler = async e => {
		e.preventDefault()
		props.setLoading(true)
		const newLocation =  { name }

		try {
            const config = {
              headers: {
                'Content-Type' : 'application/json'
              }
            }
            const body = JSON.stringify(newLocation)
            const res = await axios.post(`${URL}/locations`, body, config)
        	Swal.fire({
              title: "Success",
              text: "Successfully created new location!",
              icon: "success",
              showConfirmationButton: false,
              timer: 3000
            })
        	setIsRedirected(true)

        	return window.location = "/locations"
		} catch(e) {
			 Swal.fire({
              title: "Error",
              text: "Adding new location error",
              icon: "error",
              showConfirmationButton: false,
              timer: 3000
            })
			console.log(e)
		}
	}

	  //USE EFFECT
  useEffect(() => {
    if(name !== "") {
      setDisabledBtn(false)
    } else {
      setDisabledBtn(true)
    }
  }, [formData])

  // REDIRECT CONDITION
  if(isRedirected) {
    return <Redirect to="/locations" />
  }
  


	return (
		<Fragment>
			<Form className="mb-5 container-fluid col-10 offset-md-1 border border-light rounded" onSubmit={ e => onSubmitHandler(e) }>
				<div className=" d-flex justify-content-center mt-3">
					<h4 className="form-hdr">ADD NEW LOCATION</h4>
				</div>
				<FormGroup className="mt-1">
					<Label>Name</Label>
					<Input
						type="text"
			        	name="name"
			        	id="name"
			        	value={name}
			        	onChange={ e => onChangeHandler(e) }
			        	maxLength="30"
			        	required
					/>
				</FormGroup>
				{/*<FormGroup>
					<Label>XXXXX</Label>
					<Input
						type="text"
			        	name="Locations"
			        	id="Locations"
			        	required
					/>
				</FormGroup>
				<FormGroup className="mb-5">
					<Label>XXXXX</Label>
					<Input
						type="text"
			        	name="slot"
			        	id="slot"
			        	required

					/>
				</FormGroup>*/}
				<Button className="btn btn-block btn-primary mb-4">Save Changes</Button>
			</Form>
		</Fragment>
	)
}

export default LocationForm;