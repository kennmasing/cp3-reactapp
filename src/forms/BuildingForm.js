import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col,
	Button, Form, FormGroup, Label, Input,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';


const BuildingForm = (props) => {

const [formData, setFormData] = useState({
		locations: [],
		locationId: "",
		name: ""
		// bookings: []
	})
	const [ isRedirected, setIsRedirected ] = useState(false);
	const [ disabledBtn, setDisabledBtn ] = useState(true);

	const { locations, locationId, name } = formData;
	useEffect(() => {
		setFormData({
			locations: props.locations,
			locationId: locationId ? locationId : "",
			name: name ? name : null
		})
	},  [props])

  	const populateLocations = () => {
  		return locations.map(location => {
  			return (
  				<option
  					key={location._id}
  					value={location._id}
  					selected={locationId ? location._id : ""}
  				>
  				{location.name.toUpperCase()}
  				</option>
			)
  		})
  	}

  	const onChangeHandler = async e  => {
  		setFormData({
  			...formData,
  			[e.target.name] : e.target.value
  		})
  	}

  	const onSubmitHandler = async (e) => {
  		e.preventDefault();
  		// props.setLoading(true)
  		const newBuilding = { locationId, name }
  		try {
  			const config = {
              headers: {
              	'Authorization' : `Bearer: ${localStorage.token}`
              }
            }
            config.headers["Content-Type"] = "application/json";
  			let bodyBuilding = JSON.stringify(newBuilding)
  			const body = JSON.parse(bodyBuilding)
  			const res = await axios.post(`${URL}/buildings`, body, config)
  			console.log("NEW BUILDING RESPONSE", res)

			Swal.fire({
				title: "Success",
				text: "Building Created",
				icon: "success",
				showConfirmationButton: false,
				timer: 3000
			})


	setIsRedirected(true)
  		} catch(e) {
  			console.log(e)
  		}
  	}

	useEffect(() => {
		props.getBuildings(`/${locationId}`)
	}, [locationId])

  		  //USE EFFECT
  	useEffect(() => {
    if(locationId != null && name !== "") {
      setDisabledBtn(false)
    } else {
      setDisabledBtn(true)
    }
  }, [formData])

  	if(isRedirected) {
  		return <Redirect to="/buildings" />
  	}

  	let na;
	if(locationId === null) {
		na = (
			<option selected disabled>Select one</option>
		)
	}
	
	return (
		<Fragment>
			<Form className="mb-5 container-fluid col-10 offset-md-1 border border-light rounded" onSubmit={ e => onSubmitHandler(e) }>
				<div className=" d-flex justify-content-center mt-3">
					<h4 className="form-hdr">ADD NEW BUILDING</h4>
				</div>
				<FormGroup>
					<Label>Select Location</Label>
					<Input
						type="select"
			        	name="locationId"
			        	id="locationId"
			        	required
			        	onChange={e => onChangeHandler(e)}
			        	value={locationId}
					>
						{na}
						{populateLocations()}
					</Input>
				</FormGroup>
				<FormGroup className="mt-1">
					<Label>Building Name</Label>
					<Input
						type="text"
			        	name="name"
			        	id="name"
			        	value={name}
			        	onChange={ e => onChangeHandler(e) }
			        	maxLength="30"
			        	required
					/>
				</FormGroup>
				<Button className="btn btn-block btn-primary mb-4" disabled={disabledBtn}>Save Changes</Button>
			</Form>
		</Fragment>
	)
}

export default BuildingForm;