import React from 'react';
import axios from 'axios';
import StripeCheckout from 'react-stripe-checkout';
import { URL, PUBLISHABLE_KEY } from '../config';
// import Swal from 'sweetalert2';

const StripeForm = ({ email, amount }) => {

    const checkout = token => {

        const body = {
            // token: token,
            // amount: amount
            token,
            amount
        }

        axios
            .post(`${URL}/payments`, body)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log("Payment error", error)
            })
    }
    
    return (
        <StripeCheckout
            email={email}
            label="Card Payment"
            name="MERN Tracker App"
            description="App tagline here..."
            panelLabel="Submit"
            token={checkout}
            stripeKey={PUBLISHABLE_KEY}
            billingAddress={false}
            currency="PHP"
            allowRememberMe={false}
        />
    )

}

export default StripeForm;