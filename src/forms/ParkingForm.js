import React, { Fragment, useState, useEffect } from 'react';
import {
	Container, Row, Col,
	Button, Form, FormGroup, Label, Input,
	Card, CardImg, CardText, CardBody, CardLink, CardTitle, CardSubtitle
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';


const ParkingForm = (props) => {

const [formData, setFormData] = useState({
		locations: [],
		locationId: "",
		buildings: [],
		buildingId: "",
		name: ""
		// bookings: []
	})
	const [ isRedirected, setIsRedirected ] = useState(false);
	const [ disabledBtn, setDisabledBtn ] = useState(true);

	const { locations, locationId, buildings, buildingId, name } = formData;
	useEffect(() => {
		setFormData({
			locations: props.locations,
			locationId: locationId ? locationId : "",
			buildings: props.buildings,
			buildingId: buildingId ? buildingId : "",
			name: name ? name : null
			// bookings: props.bookings
		})
	},  [props])

  	const populateLocations = () => {
  		return locations.map(location => {
  			return (
  				<option
  					key={location._id}
  					value={location._id}
  					selected={locationId ? location._id : null}
  				>
  				{location.name.toUpperCase()}
  				</option>
			)
  		})
  	}
  	const populateBuildings = () => {
  		return buildings.map(building => {
  			return (
  				<option
  					key={building._id}
  					value={building._id}
  					selected={buildingId ? building._id : null}
  				>
  				{building.name.toUpperCase()}
  				</option>
			)
  		})
  	}

  	const onChangeHandler = async e  => {
  		setFormData({
  			...formData,
  			[e.target.name] : e.target.value
  		})
  	}

  	  const onSubmitHandler = async (e) => {
      e.preventDefault();
      const newParking = { locationId, buildingId, name }
      try {
		const config = {
          headers: {
          	'Authorization' : `Bearer: ${localStorage.token}`
          }
        }
        config.headers["Content-Type"] = "application/json";
        let bodyParking = JSON.stringify(newParking)
        const body = JSON.parse(bodyParking)
        const res = await axios.post(`${URL}/parkings`, body, config)
        console.log("NEW Parking RESPONSE", res)

      Swal.fire({
        title: "Success",
        text: "Parking Created",
        icon: "success",
        showConfirmationButton: false,
        timer: 3000
      })


        setIsRedirected(true)
      } catch(e) {
        console.log(e)
      }
    }

	useEffect(() => {
		props.getBuildings(`/${locationId}`)
	}, [locationId])
	
	useEffect(() => {
		props.getParkings(`/${buildingId}`)
	}, [buildingId])


	  useEffect(() => {
    if(locationId !== null && buildingId !== null && name !== "") {
      setDisabledBtn(false)
    } else {
      setDisabledBtn(true)
    }
  }, [formData])


  	if(isRedirected) {
  		return window.location = "/parkings"
  	}

  	// if(isRedirected) {
  	// 	return window.location = "/parkings"
  	// }

 //  	let na;
	// if(locationId === "" || buildingId === "") {
	// 	na = (
	// 		<option selected disabled>Select one</option>
	// 	)
	// }

	
	return (
		<Fragment>
			<Form className="mb-5 container-fluid col-10 offset-md-1 border border-light rounded" onSubmit={ e => onSubmitHandler(e) }>
				<div className=" d-flex justify-content-center mt-3">
					<h4 className="form-hdr">ADD NEW PARKING</h4>
				</div>
				<FormGroup>
					<Label>Select Location</Label>
					<Input
						type="select"
			        	name="locationId"
			        	id="locationId"
			        	required
			        	onChange={e => onChangeHandler(e)}
			        	value={locationId}
			        	//VALIDATION

					>
						<option selected disabled>Select one</option>
						{populateLocations()}
					</Input>
				</FormGroup>
				<FormGroup>
					<Label>Select Building</Label>
					<Input
						type="select"
			        	name="buildingId"
			        	id="buildingId"
			        	required
			        	onChange={e => onChangeHandler(e)}
			        	value={buildingId}

					>
						<option selected disabled>Select one</option>
						{populateBuildings()}
					</Input>
				</FormGroup>
				<FormGroup className="mt-1">
					<Label>Add New Parking Slot</Label>
					<Input
						type="text"
			        	name="name"
			        	id="name"
			        	value={name}
			        	onChange={ e => onChangeHandler(e) }
			        	//VALIDATION
			        	min="1"
			        	max="3"
			        	pattern="[0-9]+" 
			        	required
					/>
				</FormGroup>
				<Button className="btn btn-block btn-primary mb-4" >Save Changes</Button>
			</Form>
		</Fragment>
	)
}

export default ParkingForm;