import React, { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import axios from 'axios';
import { URL } from '../config';

import moment from 'moment';
import DatePicker from 'react-datepicker';
import { getDay, addDays, setHours, setMinutes } from 'date-fns';

import "react-datepicker/dist/react-datepicker.css";

import DateForm from './DateForm'

const DateRangeForm = (props) => {

	const [startDate, setStartDate] = useState(null);
	const [endDate, setEndDate] = useState(null);

  	//USE STATE
  	const [bookingDates, setBookingDates] = useState([])

  	//MAP AVAILABILITIES
  	useEffect(() => {
  		const arr = []
  			if(props.bookings.length > 0) {
	  			props.bookings.map(booking => {
	  				const diff = (parseInt(moment(booking.endDate).format('D')) - parseInt(moment(booking.startDate).format('D')))
					for (let i = 0; i <= diff; i++) {
						console.log("I++", i)
						arr.push(addDays(new Date(booking.startDate),  i))
					}
	  			})
  			}
  			setBookingDates(arr)
  	}, [props.bookings])

  	//SUBMIT HANDLER
	const onSubmitHandler = (e) => {
		e.preventDefault();
		
		const d = new Date(startDate.getTime())
		const html = `${d.toDateString()} ${d.toLocaleTimeString()}`


		Swal.fire({
			icon: "question",
			title: "Confirm Selected Schedule",
			html: html,
			focusConfirm: false,
			showCloseButton: true
		}).then(async (response) => {
			console.log(response)

			if(response.value) {
				
				const startDate = startDate.setSeconds(0);
				const endDate = endDate.setSeconds(0);

				const newBooking = {
						props.locationId,
						props.buildingId,
						props.parkingId,
						startDate: new Date(startDate),
						endDate: new Date(endDate),
						schedule: new Date(startDate) 
				}

				props.createBooking(newBooking, html)
				setStartDate("")
				
				// try {
				// 	const config = {
				// 		headers: {
				// 			Authorization: `Bearer ${props.token}`
				// 		}
				// 	}
				// 	// const url = "http://localhost:8000"
				// 	let bodyBooking = JSON.stringify(newBooking)
				// 	const body = JSON.parse(bodyBooking)
				// 	// const body = JSON.stringify(newBooking)
				// 	const res = await axios.post(`${URL}/bookings`, body, config)
				// 	// console.log("NEW BOOKING", res)

				// 	localStorage.setItem("bookingId", res.data._id)
				// 	localStorage.setItem("locationId", locationId)
				// 	localStorage.setItem("buildingId", buildingId)
				// 	localStorage.setItem("parkingId", parkingId)


				// 	Swal.fire({
				// 		title: "Success",
				// 		text: "Booking Created",
				// 		icon: "success",
				// 		showConfirmationButton: false,
				// 		timer: 3000
				// 	})
				// 	const updates = {}
				// 	props.disableParking(parkingId, updates)
				// 	setIsRedirected(true)
				// } catch(e) {
				// 	console.log(e.response.data)
				// }			
			}
		})

	}


  	//DATE PICKER
	let datepicker = ""
  	if(props.bookings) {
  		datepicker = (
  			<Fragment>
	  			<form onSubmit={ e => onSubmitHandler(e) }>
		  			<DatePicker
						selected={startDate}
						onChange={date => setStartDate(date)}
						selectsStart
						startDate={startDate}
						endDate={endDate}
						minDate={new Date()}
						excludeDates={bookingDates}
						className="mr-3 container-fluid"

					/>
					<DatePicker
						selected={endDate}
						onChange={date => setEndDate(date)}
						selectsEnd
						startDate={startDate}
						endDate={endDate}
						minDate={startDate}
						excludeDates={bookingDates}
						className="container-fluid"
					/>
					<button className="btn btn-success">Book</button>
				</form>
			</Fragment>
  		)
  	} else if(!props.bookings) {
  		datepicker = (
  			<Fragment>
  				<form onSubmit={ e => onSubmitHandler(e) }>
					<DatePicker
						selected={startDate}
						onChange={date => setStartDate(date)}
						selectsStart
						startDate={startDate}
						endDate={endDate}
						minDate={new Date()}
						className="mr-3 container-fluid"
					/>
					<DatePicker
						selected={endDate}
						onChange={date => setEndDate(date)}
						selectsEnd
						startDate={startDate}
						endDate={endDate}
						minDate={startDate}
						excludeDates={bookingDates}
						className="container-fluid"
					/>
					<button className="btn btn-success">Book</button>
				</form>
			</Fragment>
  		)
  	}



	return (
		<Fragment>
			{datepicker}
		</Fragment>
	)
} 

export default DateRangeForm;

