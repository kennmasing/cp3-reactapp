import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from '../config';


const UserProfileForm = (props) => {
  // console.log("M E M B E R   F O R M", props.getCurrentUserForm)
  const {
    updateUser,
    currentUser
  } = props
  
  console.log("CURRENT USER IN FORM", currentUser)


  const [userData, setUserData] = useState({
    _id: "",
    firstName: "",
    lastName: "",
    username: "",
    email: ""
  })

  const { _id, firstName, lastName, username, email } = userData;

  useEffect(() => {
    setUserData({
      _id: currentUser._id,
      firstName: currentUser.firstName,
      lastName: currentUser.lastName,
      username: currentUser.username,
      email: currentUser.email
    })
  }, [currentUser])


  const onChangeHandler = (e) => {
    setUserData({
      ...userData,
      [e.target.name] : e.target.value
    })
    console.log("CHANGE HANDLER", e.target.value)
  }



  const onSubmitHandler = async (e) => {
    e.preventDefault();

    const updates = { firstName, lastName }

    updateUser(_id, updates)
  }

  
  return (
    <Form className="container mt-3" onSubmit={ e => onSubmitHandler(e) }>
      <FormGroup>
        <Label for="firstName">First Name</Label>
        <Input
          type="text"
          name="firstName"
          id="firstName"
          value={firstName}
          onChange={ e => onChangeHandler(e) }
          //VALIDATION
          maxLength="20"
          pattern="[a-zA-Z0-9]+"
        />
      </FormGroup>
      <FormGroup>
        <Label for="lastName">Last Name</Label>
        <Input
          type="text"
          name="lastName"
          id="lastName"
          value={lastName}
          onChange={ e => onChangeHandler(e) }
          //VALIDATION
        />
      </FormGroup>
      <FormGroup>
        <Label for="username">Username</Label>
        <Input
          type="text"
          name="username"
          id="username"
          value={username}
          onChange={ e => onChangeHandler(e) }
          disabled
          //VALIDATION
          minLength="8"
        />
      </FormGroup>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input
          type="email"
          name="email"
          id="email"
          value={email}
          onChange={ e => onChangeHandler(e) }
          disabled
          //VALIDATION
        />
      </FormGroup>

      <Button color="primary" className="btn mt-4 mb-4 btn-block">Save Changes</Button>
    </Form>
  );
}

export default UserProfileForm;